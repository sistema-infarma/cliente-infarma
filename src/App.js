import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { ConfigureStore } from './redux/configureStore'; 
import './css/principal.css'
import 'antd/dist/antd.css';
import Main from './components/Main'
const store = ConfigureStore()
function App() {
  return (
    <Provider store={store}>
        <Router>
            
                <Main/>
            
        </Router>
    </Provider>
  );
}

export default App;
