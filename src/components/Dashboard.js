import React from 'react';
import { Layout, Menu, Modal} from 'antd';
import {Route} from 'react-router-dom'
import { UploadOutlined, UserOutlined, VideoCameraOutlined, DatabaseOutlined,ExclamationCircleOutlined } from '@ant-design/icons';
import { Link,BrowserRouter as Router  } from 'react-router-dom';
import '../css/Layout.css'
import { useIdleTimer } from 'react-idle-timer'

import Productos from './Productos';
import Producto from './Producto'
import Laboratorios from './Laboratorios'
import Presentaciones from './Presentaciones'

import Activos from './Activos'

const {Footer, Sider, Content } = Layout;

const { SubMenu } = Menu;

const { info } = Modal;

const Dashboard = ({logoutUser,PrivateRoute,auth}) => {

    

    const ShowConfirm=()=>{
        console.log('Se logueo desde'+getLastActiveTime())
        logoutUser();
        info({
          title: 'Se ha deslogueado de su sesion por inactividad',
          icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
          content: 'Vuelva a loguearse',
          okText:'Ok',
          onOk() {
                console.log('ok')
          }
        });
      }



    const handleOnIdle = event => {
        ShowConfirm();
      }
     
      const handleOnActive = event => {
        console.log('user is active', event)
        console.log('time remaining', getRemainingTime())
      }
     
      const handleOnAction = (e) => {
        console.log('user did something', e)
      }
     
      const { getRemainingTime, getLastActiveTime } = useIdleTimer({
        timeout: 1000*60*20,
        onIdle: handleOnIdle,
        onActive: handleOnActive,
        onAction: handleOnAction,
        debounce: 500
      })


    return (
        <Router>
            <Layout   >
                        <Sider  
                            style={{  position: 'sticky', top: 0, left: 0, zIndex:1 }}
                            className='fixed'
                            breakpoint="lg"
                            collapsedWidth="0"
                            onBreakpoint={broken => {
                                console.log(broken);
                            }}
                            onCollapse={(collapsed, type) => {
                                console.log(collapsed, type);
                            }}
                        >
                            <div>
                                <Link to='/dashboard'><img src='/img/logo-pastilla.png' alt="logo" className="responsive-img"/></Link>
                            </div>
                            
                            <Menu theme="dark" mode="inline" >
                                    
                                <SubMenu
                                    key="sub1"
                                    title={
                                        <span>
                                        <DatabaseOutlined />
                                        <span>Inventario</span>
                                        </span>
                                    }
                                    >
                                        
                                            <Menu.Item key="1">Productos
                                                <Link to='/productos'></Link>
                                            </Menu.Item>
                                        

                                        
                                            <Menu.Item key="2">Activos
                                            <Link to='/activos'></Link>
                                            </Menu.Item>
                                        

                                        
                                            <Menu.Item key="3">Laboratorios
                                            <Link to='/laboratorios'></Link>
                                            </Menu.Item>
                                        

                                        
                                            <Menu.Item key="4">Presentaciones
                                            <Link to='/presentaciones'></Link>
                                            </Menu.Item>
                                        
                                        

                                    </SubMenu>
                                    
                                    <Menu.Item key="5" icon={<VideoCameraOutlined />}>
                                        Compras
                                    </Menu.Item>
                                    <Menu.Item key="6" icon={<UploadOutlined />}>
                                        Ventas
                                    </Menu.Item>
                                    <Menu.Item key="7" icon={<UserOutlined />}>
                                        Logout
                                        <Link to='/Login' onClick={logoutUser}></Link>
                                    </Menu.Item>
                                </Menu>
                        </Sider>
                <Layout style={{height:'100vh'}}  >
                    <Content style={{ margin: '24px 16px 0',overflowX:'hidden'}}>
                            <Route exact path='/productos' component={Productos}/>

                            <Route exact path='/productos/:productoId' render={props=>{
                                let id=props.location.pathname.replace("/productos/",'')
                                return(
                                        <Producto
                                            id={id}
                                        />
                                    
                                )
                            }}/>
                            
                            <Route exact path='/laboratorios' component={Laboratorios}/>
                            <Route exact path='/presentaciones' component={Presentaciones}/>
                            <Route exact path='/activos' component={Activos}/>

                    </Content>
                    {/* <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer> */}
                </Layout>
            </Layout>
        </Router>
    );
};

export default Dashboard;