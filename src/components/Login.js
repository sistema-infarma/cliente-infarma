import React from 'react';
import { Layout } from 'antd';
import { Row, Col,Form, Input, Button,Spin } from 'antd';
import { UserOutlined, LockOutlined,LoadingOutlined} from '@ant-design/icons';
import '../css/Layout.css'
import { withRouter } from 'react-router-dom'

const {Content } = Layout;

const Login = ({loginUser,history,auth}) => {

    const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />
    const [form] = Form.useForm();
    let error;
    let loading;
    const  onFinish=(values)=>{
        console.log(values)
        console.log(history)
        loginUser(values,history)
    }

    if(auth.errMess){
        error=true;
        form.resetFields();
    }else{
        error=false
    }

    if(auth.isLoading){
        loading=true;
    }else{
        loading=false
    }

    


    const onFinishFailed=()=>{
        console.log('hola')
    }
    return (
       <Layout style={{height:"100vh"}}>
            <Content type="flex" justify="center" align="middle" style= {{alignItems: 'center'}} >
                <Row className="login-bg" type="flex" justify="center" align="middle" style={{minHeight: '100vh'}} gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }} >
                    <Col xs={20} sm={20} md={12} lg={6} xl={6}>
                        <Row type="flex" justify="center" align="middle" >
                            <Col span={24} className="bg-light" justify="center" align="middle">
                                <div>
                                    <img src='/img/logo.png' alt="logo" class="responsive-img"/>
                                </div>
                                <Form
                                    name="basic"
                                    initialValues={{ remember: true }}
                                    onFinish={onFinish}
                                    onFinishFailed={onFinishFailed}
                                    form={form}
                                >
                                    <Form.Item
                                        name="username"
                                        rules={[{ required: true, message: 'Porfavor ingrese su usuario!' }]}
                                    >
                                        <Input
                                            prefix={<UserOutlined className="site-form-item-icon" />} 
                                            placeholder="Usuario"
                                        />
                                    </Form.Item>

                                    <Form.Item
                                        name="password"
                                        rules={[{ required: true, message: 'Porfavor ingrese su contraseña!' }]}
                                    >
                                        <Input.Password
                                            prefix={<LockOutlined className="site-form-item-icon" />}
                                            type="password"
                                            placeholder="Contraseña"
                                        />
                                    </Form.Item>

                                    <Form.Item >
                                        <Button type="primary" htmlType="submit" size="large" shape="round">
                                        Login
                                        </Button>
                                    </Form.Item>
                                </Form>
                                {loading && <Spin indicator={antIcon} />}
                                {error && !loading && <p style={{color:'red'}}>Usuario o contraseña incorrectos</p>}
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Content>
        </Layout>
    );
};

export default withRouter(Login) ;