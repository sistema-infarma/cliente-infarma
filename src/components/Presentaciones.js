import React,{useEffect,useState} from 'react';
import {getPresentaciones,postPresentacion,putPresentacion} from '../redux/ActionCreators';
import {withRouter, Link } from 'react-router-dom';
import {Card,Row,Col,PageHeader,Button,Pagination,Modal,Switch} from 'antd';
import { connect } from 'react-redux';
import Loading from './Loading';
import ModalForm from './Formularios/ModalForm'
import {SearchOutlined,PlusOutlined,CloseCircleOutlined,ExclamationCircleOutlined,CheckCircleOutlined,EditOutlined } from '@ant-design/icons';
import PresentacionesForm from './Formularios/PresentacionesForm'
import ModalError from './Formularios/ModalError'
const mapStateToProps=state=>{
    return {
        presentaciones:state.presentaciones,
        presentacion:state.presentacion
    }
}

const mapDispatchToProps=(dispatch)=>({
    getPresentaciones:()=>{dispatch(getPresentaciones())},
    postPresentacion:(v,history)=>{dispatch(postPresentacion(v,history))},
    putPresentacion:(v,history)=>{dispatch(putPresentacion(v,history))}
}); 

const {confirm}=Modal;

const Presentaciones = ({presentaciones,presentacion,getPresentaciones,history,postPresentacion,putPresentacion}) => {

    const title=window.location.href.substring(27,window.location.href.length)
    useEffect(()=>{

     getPresentaciones();

    },[])

    const [pagination, setpagination] = useState({
        size:24,
        min:0,
        max:24
    })

    const [presentacions, setPresentations] = useState([])
    const [eliminados,setEliminados]=useState(false)

    const turnPage =(e)=>{
        setpagination({
            ...pagination,
            min:(e - 1) * pagination.size,
            max:e*pagination.size
        })
    }
    if(presentaciones.isLoading) return <Loading/>

    const eliminated=()=>{
        setEliminados(!eliminados)
        if(!eliminados){
            setPresentations(presentaciones.presentaciones)
        }else{
            setPresentations(presentaciones.presentaciones.filter(p=>p.activo===true))
        }
     }


    const verifyPost=(values)=>{
        confirm({
            title: 'Desea ingresar esta Presentacion?',
            icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
            okText:'Si',
            cancelText:'No',
            onCancel() {
            },
            onOk(){
                postPresentacion(values,history);
            }
            
          });
    }
    const verifyPut=(values,p)=>{
        confirm({
            title: 'Desea ingresar esta Presentacion?',
            icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
            okText:'Si',
            cancelText:'No',
            onCancel() {
            },
            onOk(){
                values.presentacionId=p._id;
                putPresentacion(values,history);
            }
            
          });
    }

    const eliminate=(id)=>{
        confirm({
          title: 'Desea eliminar este Laboratorio?',
          icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
          okText:'Si',
          cancelText:'No',
          onCancel() {
          },
          onOk(){
              let values={};
              values.presentacionId=id;
              values.activo=false;
              putPresentacion(values,history)
          }
          
        });
      }
  
      const activate=(id)=>{
        confirm({
          title: 'Desea reactivar este Laboratorio?',
          icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
          okText:'Si',
          cancelText:'No',
          onCancel() {
          },
          onOk(){
              let values={};
              values.presentacionId=id;
              values.activo=true;
              putPresentacion(values,history)
          }
          
        });
      }
    let data=[]

    if(presentacions.length>0){
        data=presentacions
    }else{
        data=presentaciones.presentaciones.filter(p=>p.activo===true);
    }
    return (
        <React.Fragment>
            <Row type="flex" justify="right" align="middle">
                <PageHeader
                    className="font-title2"
                    title={(title).charAt(0).toUpperCase()+title.slice(1)}
                />

                <ModalForm title='Nueva Presentacion'>
                    <PresentacionesForm sendValues={verifyPost}/>
                    {presentacion.errMess && <ModalError>{presentacion.errMess}</ModalError>}
                </ModalForm>

                <Switch  onChange={eliminated}  style={{marginLeft:'auto',marginRight:'10px'}} checkedChildren="Eliminados" unCheckedChildren="Eliminados" />
            </Row>
            <Row gutter={[16,16]} type="flex" justify="left" align="middle">
                {data.slice(pagination.min,pagination.max).map(p=>{
                    let color='';
                    if(!p.activo) color='#ffdbd9'
                    return(
                        <Col xs={12} sm={12} md={7} lg={4}>
                        
                            <Card headStyle={{textAlign:'center'}} style={{backgroundColor:color}} title={p.tipo}>
                                <Row type="flex" justify="center" align="middle">
                                    <ModalForm title='Modificar Presentacion' icon={<EditOutlined/>} ghost={true}><PresentacionesForm sendValues={(values)=>verifyPut(values,p)} pres={p}/></ModalForm>
                                    {p.activo ?
                                        <Button onClick={()=>(eliminate(p._id))}   type="text" style={{marginLeft:'10px'}}  shape="circle" icon={<CloseCircleOutlined style={{fontSize:'30px',color:'red'}} />}  />:
                                        <Button onClick={()=>(activate(p._id))}  type="text" style={{marginLeft:'10px'}}  shape="circle" icon={<CheckCircleOutlined style={{fontSize:'30px',color:'green'}} />}  />
                                    }
                                </Row>
                            </Card>
                        </Col>
                    );
                })}
            </Row>

            <Row type="flex" justify="right" align="middle">

                <Col span={24} style={{textAlign:'right', padding:'0 10px 10px 0'}}>
                    <Pagination defaultCurrent={1}  total={presentaciones.presentaciones.length} pageSize={pagination.size} onChange={turnPage} />
                </Col>
                
            </Row>
        </React.Fragment>
    );
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Presentaciones));