import React,{useEffect,useState} from 'react';
import {getActivos,postActivo,putActivo} from '../redux/ActionCreators';
import {withRouter, Link } from 'react-router-dom';
import {Card,Row,Col,PageHeader,Button,Pagination,Modal,Switch} from 'antd';
import { connect } from 'react-redux';
import Loading from './Loading';
import ModalForm from './Formularios/ModalForm'
import {SearchOutlined,PlusOutlined,CloseCircleOutlined,ExclamationCircleOutlined,CheckCircleOutlined,EditOutlined } from '@ant-design/icons';
import ActivosForm from './Formularios/ActivosForm'
import ModalError from './Formularios/ModalError'
const mapStateToProps=state=>{
    return {
        activos:state.activos,
        activo:state.activo
    }
}

const mapDispatchToProps=(dispatch)=>({
    getActivos:()=>{dispatch(getActivos())},
    postActivo:(v,history)=>{dispatch(postActivo(v,history))},
    putActivo:(v,history)=>{dispatch(putActivo(v,history))} 
}); 

const {confirm}=Modal;

const Activos = ({activos,activo,getActivos,putActivo,postActivo,history}) => {

    const title=window.location.href.substring(27,window.location.href.length)
    useEffect(()=>{

        getActivos();

    },[])

    const [pagination, setpagination] = useState({
        size:24,
        min:0,
        max:24
    })

    const [acts, setActivos] = useState([])
    const [eliminados,setEliminados]=useState(false)

    const turnPage =(e)=>{
        setpagination({
            ...pagination,
            min:(e - 1) * pagination.size,
            max:e*pagination.size
        })
    }
    if(activos.isLoading) return <Loading/>

    const eliminated=()=>{
        setEliminados(!eliminados)
        if(!eliminados){
            setActivos(activos.activos)
        }else{
            setActivos(activos.activos.filter(a=>a.activo===true))
        }
     }


    const verifyPost=(values)=>{
        confirm({
            title: 'Desea ingresar esta Activo?',
            icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
            okText:'Si',
            cancelText:'No',
            onCancel() {
            },
            onOk(){
                 postActivo(values,history); 
            }
            
          });
    }
    const verifyPut=(values,a)=>{
        confirm({
            title: 'Desea ingresar esta Presentacion?',
            icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
            okText:'Si',
            cancelText:'No',
            onCancel() {
            },
            onOk(){
                values.activoId=a._id;
                putActivo(values,history);
            }
            
          });
    }

    const eliminate=(id)=>{
        confirm({
          title: 'Desea eliminar este Laboratorio?',
          icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
          okText:'Si',
          cancelText:'No',
          onCancel() {
          },
          onOk(){
              let values={};
              values.activoId=id;
              values.activo=false;
              putActivo(values,history)
          }
          
        });
      }
  
      const activate=(id)=>{
        confirm({
          title: 'Desea reactivar este Activo?',
          icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
          okText:'Si',
          cancelText:'No',
          onCancel() {
          },
          onOk(){
              let values={};
              values.activoId=id;
              values.activo=true;
              putActivo(values,history)
          }
          
        });
      }
    let data=[]

    if(acts.length>0){
        data=acts;
    }else{
        data=activos.activos.filter(a=>a.activo===true);
    }
    return (
        <React.Fragment>
            <Row type="flex" justify="right" align="middle">
                <PageHeader
                    className="font-title2"
                    title={(title).charAt(0).toUpperCase()+title.slice(1)}
                />

                <ModalForm title='Nuevo Activo'>
                     <ActivosForm sendValues={verifyPost}/>
                     {activo.errMess && <ModalError>{activo.errMess}</ModalError>} 
                </ModalForm>

                <Switch  onChange={eliminated}  style={{marginLeft:'auto',marginRight:'10px'}} checkedChildren="Eliminados" unCheckedChildren="Eliminados" />
            </Row>
            <Row gutter={[16,16]} type="flex" justify="left" align="middle">
                {data.slice(pagination.min,pagination.max).map(a=>{
                    let color='';
                    if(!a.activo) color='#ffdbd9'
                    return(
                        <Col xs={12} sm={12} md={7} lg={6} style={{display:'inline-flex',alignSelf:'stretch'}}  >
                        
                            <Card headStyle={{textAlign:'center'}} style={{backgroundColor:color}} title={a.nombreComercial}>

                                <Row type="flex" justify="center" align="middle">
                                    <Col span={24} style={{textAlign:'center'}}>
                                        <p><b>Productos Registrados: </b>{a.genericoCount}</p>
                                        <p>{a.descripcion}</p> 
                                    </Col>

                                    <Col span={24} style={{display:'flex', justifyContent:'center', alignItems:'center'}}>
                                        <ModalForm title='Modificar Activo' icon={<EditOutlined/>} ghost={true}> <ActivosForm sendValues={(values)=>verifyPut(values,a)} act={a}/></ModalForm>

                                        {a.activo ?
                                            <Button onClick={()=>(eliminate(a._id))}   type="text" style={{marginLeft:'10px'}}  shape="circle" icon={<CloseCircleOutlined style={{fontSize:'30px',color:'red'}} />}  />:
                                            <Button onClick={()=>(activate(a._id))}  type="text" style={{marginLeft:'10px'}}  shape="circle" icon={<CheckCircleOutlined style={{fontSize:'30px',color:'green'}} />}  />
                                        }
                                    </Col>
                                </Row>

                            </Card>
                        </Col>
                    );
                })}
            </Row>

            <Row type="flex" justify="right" align="middle">

                <Col span={24} style={{textAlign:'right', padding:'0 10px 10px 0'}}>
                    <Pagination defaultCurrent={1}  total={activos.activos.length} pageSize={pagination.size} onChange={turnPage} />
                </Col>
                
            </Row>
        </React.Fragment>
    );
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Activos));