import React,{useRef,useEffect,useState} from 'react';
import {Input,Table,Button,PageHeader ,Space,Row,Col,Switch,Modal,message} from 'antd';
import Highlighter from 'react-highlight-words';
import ModalForm from './Formularios/ModalForm'
import ModalError from './Formularios/ModalError'
import {SearchOutlined,PlusOutlined,CloseCircleOutlined,ExclamationCircleOutlined,CheckCircleOutlined,EditOutlined } from '@ant-design/icons';

import {getLaboratorios,postLabortatorio,putLabortatorio} from '../redux/ActionCreators';
import {withRouter, Link } from 'react-router-dom';

import { connect } from 'react-redux';
import Loading from './Loading';

import LaboratorioForm from './Formularios/LaboratorioForm';

const {confirm}=Modal

const mapStateToProps=state=>{
    return {
        laboratorios:state.laboratorios,
        laboratorio:state.laboratorio
    }
}

const mapDispatchToProps=(dispatch)=>({
    getLaboratorios:()=>{dispatch(getLaboratorios())},
    postLabortatorio:(v,history)=>{dispatch(postLabortatorio(v,history))},
    putLabortatorio:(v,history)=>{dispatch(putLabortatorio(v,history))}
}); 


const Laboratorios = ({laboratorios,getLaboratorios,history,postLabortatorio,putLabortatorio,laboratorio}) => {

    const title=window.location.href.substring(27,window.location.href.length)
    const searchInput = useRef(null);

    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');
    const [change, setChange]=useState(false)
    const [eliminados,setEliminados]=useState(false)
    const [labs,setLabs]=useState([])
    useEffect(() => {
        getLaboratorios();
        
    }, [])


    if(laboratorios.isLoading)return <Loading/>


    const eliminated=()=>{
      setEliminados(!eliminados)
      if(!eliminados){
        setLabs(laboratorios.laboratorios)
      }else{
        setLabs(laboratorios.laboratorios.filter(p=>p.activo===true))
      }
   }

    const eliminate=(id)=>{
      confirm({
        title: 'Desea eliminar este Laboratorio?',
        icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
        okText:'Si',
        cancelText:'No',
        onCancel() {
        },
        onOk(){
            let values={};
            values.labId=id;
            values.activo=false;
            putLabortatorio(values,history)
        }
        
      });
    }

    const activate=(id)=>{
      confirm({
        title: 'Desea reactivar este Laboratorio?',
        icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
        okText:'Si',
        cancelText:'No',
        onCancel() {
        },
        onOk(){
            let values={};
            values.labId=id;
            values.activo=true;
            putLabortatorio(values,history)
        }
        
      });
    }
    
    const verifyPost=(values)=>{
        confirm({
            title: 'Desea ingresar este Laboratorio?',
            icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
            okText:'Si',
            cancelText:'No',
            onCancel() {
            },
            onOk(){
                postLabortatorio(values,history)
            }
            
          });
    }

    const verifyPut=(values)=>{
      confirm({
          title: 'Desea modificar este Laboratorio?',
          icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
          okText:'Si',
          cancelText:'No',
          onCancel() {
          },
          onOk(){
            putLabortatorio(values,history)
          }
          
        });
  }

    const verifyEdit=(values,record)=>{
        if(values.nombreLaboratorio !== record.nombre || values.correoLaboratorio !== record.correo || values.numeroTelefono !==record.telefono){
          setChange(false)

          values.labId=record.key

          verifyPut(values)
        }else{
          setChange(true)
        }
    }

    const getColumnSearchProps= (dataIndex)=> {
        return {
          filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
              <Input

                ref={ searchInput }
                placeholder={`Search ${dataIndex}`}
                value={selectedKeys[0]}
                onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                style={{ width: 188, marginBottom: 8, display: 'block' }}
              />
              <Space>
                <Button
                  type="primary"
                  onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                  icon={<SearchOutlined />}
                  size="small"
                  style={{ width: 90 }}
                >
                  Search
                </Button>
                <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                  Reset
                </Button>
              </Space>
            </div>
          ),
          filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
          onFilter: (value, record) =>
            record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
          onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => searchInput.current.select());
            }
          },
          render: text =>
            searchedColumn === dataIndex ? (
              <Highlighter
                highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                searchWords={[searchText]}
                autoEscape
                textToHighlight={text.toString()}
              />
            ) : (
              text
            ),
        }
      };

      const handleSearch=(selectedKeys, confirm, dataIndex)=>{
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
      };
    
      const handleReset=(clearFilters)=>{
        clearFilters();
        setSearchText('');
      };
    
      const columns=[
        {
            title: 'Nombre',
            dataIndex: 'nombre',
            key: 'nombre',
            ...getColumnSearchProps('nombre')
        },
        {
            title: 'Telefono',
            dataIndex: 'telefono',
            key: 'telefono', 
            ...getColumnSearchProps('telefono')
        },
        {
            title: 'Correo',
            dataIndex: 'correo',
            key: 'correo', 
            ...getColumnSearchProps('correo')
        },
        {
            title: 'Editar',
            key: 'editar',
            render: (text, record) => (
              <Space size="middle">
                  <ModalForm title='Nuevo laboratorio' title='Modificar Detalle' icon={<EditOutlined/>} ghost={true}>
                    <LaboratorioForm sendValues={(values)=>verifyEdit(values,record)} lab={record}/>
                    {change && <ModalError>Porfavor Realize un cambio</ModalError>}
                    {laboratorio.errMess && <ModalError>{laboratorio.errMess}</ModalError>}
                  </ModalForm>
              </Space>
            ),
        },
        {
          title: 'Eliminar',
          key: 'eliminar',
          render: (text, record) => (
            <Space style={{textAlign:'center'}} size="middle">
              {record.active ?
              <Button onClick={()=>(eliminate(record.key))}  type="text" style={{marginLeft:'30%'}}  shape="circle" icon={<CloseCircleOutlined style={{fontSize:'30px',color:'red'}} />}  />:
              <Button onClick={()=>(activate(record.key))}  type="text" style={{marginLeft:'30%'}}  shape="circle" icon={<CheckCircleOutlined style={{fontSize:'30px',color:'green'}} />}  />
              }
            </Space>
          ),
        }
    ]

    let rows=[]

   if(labs.length>0) {

      labs.forEach(laboratorio=>{
          rows.push({
                  key:laboratorio._id,
                  nombre:laboratorio.nombreLaboratorio,
                  telefono:laboratorio.numeroTelefono,
                  correo:laboratorio.correoLaboratorio,
                  active:laboratorio.activo
              })
      })
      
   }else{
      laboratorios.laboratorios.filter(lab=>lab.activo===true).forEach(laboratorio=>{
          rows.push({
                  key:laboratorio._id,
                  nombre:laboratorio.nombreLaboratorio,
                  telefono:laboratorio.numeroTelefono,
                  correo:laboratorio.correoLaboratorio,
                  active:laboratorio.activo
              })
      })   
   }

    console.log(rows);


    return (
        <React.Fragment>
              <Row type="flex" justify="right" align="middle">
                  
                      <PageHeader
                          className="font-title"
                          title={(title).charAt(0).toUpperCase()+title.slice(1)}
                      />
                       
                      <ModalForm title='Nuevo laboratorio'><LaboratorioForm sendValues={verifyPost}/></ModalForm>
                      <Switch  onChange={eliminated} style={{marginLeft:'auto',marginRight:'10px'}} checkedChildren="Eliminados" unCheckedChildren="Eliminados" />
                  
              </Row>

              <Table pagination={{pageSize:6}}  rowClassName={(record, index)=>(!record.active && 'red')} columns={columns} dataSource={rows} scroll={{ x: 600}} />
        </React.Fragment>
    );
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Laboratorios));;