import React,{useState} from 'react';
import {Collapse,Descriptions,Row,Col,Input,Pagination,message,Modal,Button,Switch} from 'antd'
import {SearchOutlined}  from  '@ant-design/icons';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';
import {EditOutlined,ExclamationCircleOutlined,CloseCircleOutlined,CheckCircleOutlined} from '@ant-design/icons';
import moment from 'moment'
import ModalError from '../Formularios/ModalError';
import {putGenericoDetalles} from '../../redux/ActionCreators'


import {withRouter} from 'react-router-dom';
import { connect } from 'react-redux';


import DetallesForm from '../Formularios/DetallesForm';
import ModalForm from '../Formularios/ModalForm';
const {Panel}=Collapse;
const {confirm}=Modal;
const mapStateToProps=state=>{
    return {
        product:state.producto
    }
}

const mapDispatchToProps=(dispatch)=>({
    putGenericoDetalles:(v,history,message)=>{
        dispatch(putGenericoDetalles(v,history,message)) 
    },
}); 


const useStyles = makeStyles(theme => ({
    underline: {
        "&&&:before": {
          borderBottom: "none"
        },
        "&&:after": {
          borderBottom: "none"
        },
        "&:hover:not(.Mui-disabled):not(.Mui-focused):not(.Mui-error):before":{
            background:'none'
        },
        '&:focus':{
            background:'none'  
        },   
        '&:active':{
            background:'none'  
        }   
    },
    icon: {
        fill: 'white'
    },
    colorWhite:{
        color:'white'
    }
}));


const Detalles = ({detalles,history,putGenericoDetalles,product,idProducto}) => {

  /*   detalles.forEach(detalle => {
            detalle.tipo = detalle.tipo.tipo
            detalle.laboratorio= detalle.laboratorio.nombreLaboratorio
    });  */

    const classes=useStyles();
    
    const verify=(values)=>{
        confirm({
            title: values.text?values.text:'Desea modificar este detalle?',
            icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
            okText:'Si',
            cancelText:'No',
            onCancel() {
            },
            onOk(){
                putGenericoDetalles(values,history,message);
            },
            
          });
      }

    const [search, SetSearch] = useState({
        filter:[...detalles.filter(detalle=>detalle.activo===true)],
        option:''
    })
    const [pagination, setpagination] = useState({
        size:3,
        min:0,
        max:3
    })
    const [change, setChange]=useState(false)
    const[eliminados,setEliminados]=useState(true);

    const turnPage =(e)=>{
        setpagination({
            ...pagination,
            min:(e - 1) * pagination.size,
            max:e*pagination.size
        })
    }

    const option =(e)=>{
        console.log(e.target.value)
        SetSearch({
            ...search,
            option:e.target.value
        })
    }

    const Buscar =(e)=>{
        
       if(e!==''){
        let filtrado= filter(search.option,e);

        SetSearch({
             ...search,
             filter:[...filtrado]
         })
       }else{
            SetSearch({
                ...search,
                filter:[...detalles]
            }) 
       }
    }

    const eliminated=()=>{
        setEliminados(!eliminados);
        console.log(eliminados)
        if(eliminados){
            SetSearch({
                ...search,
                filter:[...detalles]
            })
        }else{
            SetSearch({
                ...search,
                filter:[...detalles.filter(detalle=>detalle.activo===true)]
            })
        }
    }


    const postEdit=(values,detalle)=>{

       if(detalle.cantidad !== values.cantidad || detalle.preciodeCompra !== values.preciodeCompra ||
        detalle.preciodeVenta !== values.preciodeVenta || detalle.limitedeMeses !== values.limitedeMeses ||
        detalle.metodoImportacion !== values.metodoImportacion || !moment(detalle.fechadeIngreso).isSame(values.fechadeIngreso, 'day') ||
        !moment(detalle.fechadeVencimiento).isSame(values.fechadeVencimiento, 'day') || detalle.tipo.tipo !== values.tipo || 
        detalle.laboratorio._id!== values.laboratorio || detalle.formulacion!==values.formulacion){

            values.detalleId=detalle._id;
            values.genericoId=idProducto;
            console.log(values);
            verify(values)

        }else{
            setChange(true)
        }
  
       
    }
    const changeDetailState=(values,detalle)=>{
        values.genericoId=idProducto;
        values.detalleId=detalle._id;
        console.log(values.activo)
        if(!values.activo) values.text='Desea eliminar este detalle?'; else values.text='Desea reactivar este detalle?'
        verify(values)
    }
    const filter=(option,e)=>{
        switch(option){
            case 0: 
                return detalles.filter(detalle=>detalle.codigoProductoPresentacion.toLowerCase().includes(e.toLowerCase()))
            
            case 1:
               return detalles.filter(detalle=>detalle.cantidad.toString().toLowerCase().includes(e.toLowerCase()))
            

            case 2:
                return detalles.filter(detalle=>detalle.preciodeCompra.toString().toLowerCase().includes(e.toLowerCase()))
            

            case 3:
                return detalles.filter(detalle=>detalle.preciodeVenta.toString().toLowerCase().includes(e.toLowerCase()))
            

            case 4:
                return detalles.filter(detalle=>detalle.limitedeMeses.toString().toLowerCase().includes(e.toLowerCase()))
            

            case 5:
                return detalles.filter(detalle=>detalle.metodoImportacion.toLowerCase().includes(e.toLowerCase()))
            

            case 6:
                return detalles.filter(detalle=>detalle.fechadeIngreso.toLowerCase().includes(e.toLowerCase()))
            

            case 7:
                return detalles.filter(detalle=>detalle.fechadeVencimient.toLowerCase().includes(e.toLowerCase()))
            

            case 8:
                return detalles.filter(detalle=>detalle.tipo.tipo.toLowerCase().includes(e.toLowerCase()))
            

            case 9:
                return detalles.filter(detalle=>detalle.laboratorio.nombreLaboratorio.toLowerCase().includes(e.toLowerCase()))
            
            case 10:
                return detalles.filter(detalle=>detalle.formulacion.toLowerCase().includes(e.toLowerCase()))
            
            
            default:
                return detalles.filter(detalle=>detalle.codigoProductoPresentacion.toLowerCase().includes(e.toLowerCase()))
            
        }
    }


    return (
        <React.Fragment >

            <Row  type="flex" justify="center" align="middle" >
                <h2 style={{fontSize:'35px',marginBottom:0}}>Lotes</h2>
                
            </Row>

            <Row  type="flex" justify="center" align="middle" >
                <Switch onChange={eliminated} checkedChildren="Eliminados" unCheckedChildren="Eliminados" />
                
            </Row>
            
                

           

            <Row className='product-bg border-radius' type="flex" justify="center" align="middle" style={{margin:'30px'}}>

                <Col xs={24} sm={24} md={24} lg={18}>

                    <Input className='input' bordered={false}  suffix={<SearchOutlined style={{color:'white'}}  />} onChange={(e)=>Buscar(e.target.value)}/>       
                
                </Col>
                
                <Col className='margin-top-10px' flex='auto'>
                    
                    <Select  className={classes.underline} style={{minWidth:'100%',textAlign: "center"}} value={search.option}
                        displayEmpty
                        onChange={option}
                        inputProps={{
                            classes: {
                                root: classes.colorWhite,
                                icon: classes.icon,
                            },
                        }}
                        >
                        <MenuItem value="" disabled>
                            Seleccione
                        </MenuItem>
                        <MenuItem   key={0} value={0} >Codigo</MenuItem>
                        <MenuItem key={1} value={1} >Cantidad</MenuItem>
                        <MenuItem key={2} value={2} >Precio de Compra</MenuItem>
                        <MenuItem key={3} value={3} >Precio de Venta</MenuItem>
                        <MenuItem key={4} value={4} >Meses para vender</MenuItem>
                        <MenuItem key={5} value={5} >Metodo de Importacion</MenuItem>
                        <MenuItem key={6} value={6} >Fecha de Ingreso</MenuItem>
                        <MenuItem key={7} value={7} >Fecha de Vencimiento</MenuItem>
                        <MenuItem key={8} value={8} >Presentacion</MenuItem>
                        <MenuItem key={9} value={9} >Farmacia</MenuItem>
                        <MenuItem key={10} value={10} >Formulación</MenuItem>
                    </Select>
                
                </Col>
            </Row>
            
            <Row  style={{marginBottom:'20px'}} type="flex" justify="center" align="middle">
                <Col span={24}>
                    <Collapse defaultActivevalue={['1']} >
                        {search.filter.slice(pagination.min,pagination.max).map(detalle=>{

                            let color='';
                            if(!detalle.activo) color='#ffdbd9';


                            return <Panel header={`Codigo de Caja: ${detalle.codigoCaja} ${!detalle.activo ? '(eliminado)':''}`} style={{backgroundColor:color}}  value={detalle._id}>
                                <Descriptions 
                                title='Detalles'
                                extra={
                                    <Row type="flex" justify="right" align="middle" style={{minWidth:'95px'}}>
                                        <ModalForm
                                            title='Modificar Detalle'
                                            icon={<EditOutlined/>}
                                            ghost={true}
                                            
                                        >
                                            <DetallesForm edit={true} sendValues={(value)=>{postEdit(value,detalle)}} detalle={detalle}/>

                                            {product.errMess && <ModalError>{product.errMess}</ModalError>}
                                            {change && <ModalError>Realice un cambio porfavor</ModalError> }
                                        
                                        </ModalForm>

                                        {detalle.activo ?<Button onClick={()=>{let values={}; values.activo=false; changeDetailState(values,detalle)}} type="text" style={{marginLeft:'10px'}}  shape="circle" icon={<CloseCircleOutlined style={{fontSize:'30px',color:'red'}} />}  />:
                                        <Button onClick={()=>{let values={}; values.activo=true; changeDetailState(values,detalle)}} type="text"  shape="circle" style={{marginLeft:'10px'}}  icon={<CheckCircleOutlined style={{fontSize:'30px',color:'green'}} />}  />}
                                        
                                    </Row>
                                }
                                layout="vertical" 
                                bordered={true} >
                                        <Descriptions.Item label='Codigo'>{detalle.codigoProductoPresentacion}</Descriptions.Item>
                                        <Descriptions.Item label='Cantidad'>{detalle.cantidad}</Descriptions.Item>
                                        <Descriptions.Item label='Precio de Compra:'>{detalle.preciodeCompra}</Descriptions.Item>
                                        <Descriptions.Item label='Precio de Venta:'>{detalle.preciodeVenta}</Descriptions.Item>
                                        <Descriptions.Item label='Meses para vender:'>{detalle.limitedeMeses}</Descriptions.Item>
                                        <Descriptions.Item label='Metodo de Importacion:'>{detalle.metodoImportacion}</Descriptions.Item>
                                        <Descriptions.Item label='Fecha de Ingreso:'>{detalle.fechadeIngreso.substring(0,10)}</Descriptions.Item>
                                        <Descriptions.Item label='Fecha de Vencimiento:'>{detalle.fechadeVencimiento.substring(0,10)}</Descriptions.Item>
                                        <Descriptions.Item label='Presentacion:'>{detalle.tipo.tipo}</Descriptions.Item>
                                        <Descriptions.Item label='Farmacia:'>{detalle.laboratorio.nombreLaboratorio}</Descriptions.Item>
                                        <Descriptions.Item label='Formulación:'>{detalle.formulacion}</Descriptions.Item>
                                </Descriptions>
                            </Panel>
                        })}
                    </Collapse> 
                </Col>
            </Row>
            <Row type="flex" justify="right" align="middle">
                <Col span={24} style={{textAlign:'right', padding:'0 10px 10px 0'}}>
                    <Pagination defaultCurrent={1} total={detalles.length} pageSize={pagination.size} onChange={turnPage}/>
                </Col>
            </Row>
        </React.Fragment>
        
    );
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Detalles));