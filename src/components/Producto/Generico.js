import React,{useState} from 'react';
import {Row,Col,message ,Modal} from 'antd'
import ModalForm from '../Formularios/ModalForm';
import GeneralForm from '../Formularios/GeneralForm';
import ModalError from '../Formularios/ModalError'
import {EditOutlined,ExclamationCircleOutlined} from '@ant-design/icons';

import {putGenerico,putProducto} from '../../redux/ActionCreators'


import {withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
const {confirm}=Modal
const mapStateToProps=state=>{
    return {
        product:state.producto
    }
}

const mapDispatchToProps=(dispatch)=>({
    putGenerico:(v,history,message)=>{
        dispatch(putGenerico(v,history,message)) 
    },
    putProducto:(v,history,message)=>{
        dispatch(putProducto(v,history,message)) 
    }
}); 
const Generico = ({producto,product,putGenerico,putProducto,history}) => {

    console.log(producto)
    const [change,setChange]=useState(true)


    const verify=(values)=>{
        confirm({
            title: 'Desea ingresar este producto?',
            icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
            okText:'Si',
            cancelText:'No',
            onCancel() {
            },
            onOk(){
                postEdit(values)
            },
            
          });
      }



    const postEdit =(values)=>{
        if(!values.newActivo){
            if(producto.nombreGenerico !== values.nombreGenerico ||producto.producto._id!== values.producto){
                    values.genericoId=producto._id;
                    putGenerico(values,history,message)
                    setChange(true);
            }else{
                setChange(false);
            }
        }else{
            if(producto.producto.nombreComercial !== values.nombreComercial 
            || producto.producto.descripcion !== values.descripcion){

                values.productoId=producto.producto._id
                if(producto.nombreGenerico !== values.nombreGenerico){

                    values.genericoModified=true
                    putProducto(values,history,message)

                }else{
                    putProducto(values,history,message)
                }
                setChange(true);
            }else{
                setChange(false);
            }
        }
        
        
    }

    
    return (
        <Row  className="title-producto row-border-bottom" type="flex" justify="center" align="middle">
            <Col span={24} style={{textAlign:'center'}}>
                <Row type="flex" justify="center" style={{alignItems:'center'}} align="middle">

                    <h1 >{producto.nombreGenerico}</h1>

                    <Col  lg={2} xs={24} sm={24} md={2}>
                        <ModalForm 
                            buttonSize='55px' 
                            buttonShape='round' 
                            buttonType='text' 
                            buttonColor='white' 
                            ghost={true} 
                            title='Modificar Producto'
                            icon={<EditOutlined className='edit-button-icon' />}
                        >
                            <GeneralForm buttomText='Guardar' sendValues={verify} product={producto} />
                            {!change && <ModalError>Porfavor realice un cambio</ModalError>}
                            {product.errMess&& <ModalError>{product.errMess}</ModalError>}
                        </ModalForm>
                        
                    </Col>
                </Row>
            </Col >
            <Col span={24} className='margin-top-10px' style={{textAlign:'center'}}>
                <h2>{producto.codigoProducto}</h2>
            </Col>
        </Row>
    );
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Generico));