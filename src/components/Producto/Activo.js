import React from 'react';
import {Row,Col} from 'antd'
import {ExperimentOutlined,QuestionCircleOutlined } from '@ant-design/icons';
const Activo = ({activo}) => {
    return (
        <Row className='title-producto'  style={{marginTop:'20px'}}  type="flex" justify="center" align="middle">
            <Col xs={24} sm={24} md={12} lg={12} style={{textAlign:'center',color:'white'}}>
                <ExperimentOutlined style={{ fontSize: 60}} />
                    <h2>{activo.nombreComercial}</h2>     
            </Col>

            <Col className='title-producto' xs={24} sm={24} md={12} lg={12} style={{textAlign:'center',color: 'white'}}>
                    <QuestionCircleOutlined style={{ fontSize: 60 }} />
                    <h2>{activo.descripcion}</h2>
                
            </Col>
        </Row>
    );
};

export default Activo;