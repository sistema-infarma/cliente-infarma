import React from 'react';
import { Form, Input, Button,Row,Col,Switch} from 'antd';
const ActivosForm = ({sendValues,act}) => {

    
    return (
        <Form
            onFinish={sendValues}
        >
            <Form.Item
                name="nombreComercial"
                initialValue={act && act.nombreComercial}
                rules={[
                    {
                    required: true,
                    message: 'Escriba el nombre de el activo',
                    },
                ]}
            >
                <Input
                    placeholder="Nombre del Activo"
                />
            </Form.Item>

            <Form.Item
                name="descripcion"
                initialValue={act && act.descripcion}
                rules={[
                    {
                    required: true,
                    message: 'Escriba la descricion del Activo',
                    },
                ]}
            >
                <Input
                    placeholder="Descripción del Activo"
                />
            </Form.Item>

            <Row type="flex" justify="center" align="middle">
                <Button  style={{marginBottom:'10px'}}  type="primary" htmlType="submit">
                    Ingresar
                </Button>     
            </Row>

        </Form>
    );
};

export default ActivosForm ;