import React from 'react';
import { Form, Input, Button,Row,Col,Switch} from 'antd';
const PresentacionesForm = ({sendValues,pres}) => {

    
    return (
        <Form
            onFinish={sendValues}
        >
            <Form.Item
                name="tipo"
                initialValue={pres && pres.tipo}
                rules={[
                    {
                    required: true,
                    message: 'Escriba el nombre de la presentacion',
                    },
                ]}
            >
                <Input
                    placeholder="Nombre de Presentación"
                />
            </Form.Item>

            <Row type="flex" justify="center" align="middle">
                <Button  style={{marginBottom:'10px'}}  type="primary" htmlType="submit">
                    Ingresar
                </Button>     
            </Row>

        </Form>
    );
};

export default PresentacionesForm;