import React,{useState} from 'react';
import { Modal, Button ,Row} from 'antd';
import {PlusOutlined} from '@ant-design/icons';
const ModalForm= ({children,title,buttonText,buttonColor,buttonType,ghost,icon,buttonSize}) => {

    const [modal, setModal] = useState(false)
    const showModal=()=>{
        setModal(true);
    }
 
    const handleOk =(e)=>{
        console.log(e)
    }

    const handleCancel =(e)=>{
        setModal(false);
    }

    return (

        <Row type="flex" justify="center" align="middle">
            <Button type={buttonType?buttonType:'primary'} style={{color:buttonColor,width:'auto',height:buttonSize}}  icon={icon?icon:<PlusOutlined />} ghost={ghost} shape={"circle"} size='medium' onClick={showModal}>{buttonText}</Button>
            
            <Modal
                visible={modal}
                title={title}
                onOk={handleOk}
                onCancel={handleCancel}
                footer={null}
            >
                {children}
                
            </Modal>
        </Row>
    );
};

export default ModalForm;