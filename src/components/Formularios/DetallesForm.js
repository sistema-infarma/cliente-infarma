import React,{useState, useEffect} from 'react';
import { Form, Input, Button,Row,Col,DatePicker,InputNumber,Switch, Select as Dropbox} from 'antd';
import {getPresentaciones,getLaboratorios} from '../../redux/ActionCreators'
import {withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import {CheckCircleOutlined} from '@ant-design/icons'
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core/styles';


import moment from 'moment'
const {Option}=Dropbox;
const mapStateToProps=state=>{
    return {
        presentaciones:state.presentaciones,
        laboratorios:state.laboratorios
    }
}

const mapDispatchToProps=(dispatch)=>({
    getPresentaciones:()=>{dispatch(getPresentaciones())},
    getLaboratorios:()=>{dispatch(getLaboratorios())}

}); 



const useStyles = makeStyles(theme => ({
    underline: {
         '&&&:before':{
            border:'none'
         },
          '&&:after':{
            borderColor:'#1890FF' 
        },
        '&:hover':{
            border:'none'
        }
    },
    root:{
        fontFamily:'Segoe UI',
        fontSize:'14px',
        border: '1px solid #D9D9D9',
        padding:'5px 24px 6px 0px'
    },
    icon:{
        fill: '#D9D9D9'
    },
    gray:{
        color:'#BFBFBF'
            
    }
}))

const Placeholder=({children})=>{
    const classes=useStyles();
    return <div className={classes.gray}>{children}</div>;
}

const DetallesForm = ({next,prev,sendValues,minusCount,presentaciones,laboratorios,getPresentaciones,getLaboratorios,edit,detalle}) => {

    const classes=useStyles();
    const [form] = Form.useForm();
    const [select, setSelect] = useState({
        presentacion:'',
        laboratorio:'',
        importacion:'',
        formulacion:'mg'
    });
    const [vencimiento,setVencimiento]=useState(detalle?detalle.fechadeVencimiento:'');
    const [ingreso,setIngreso]=useState(detalle?detalle.fechadeIngreso:'');
    const [activo, setActivo] = useState(false)
 
    useEffect(()=>{
        getPresentaciones();
        getLaboratorios();
    },[])
    console.log(detalle)
    const nuevo =()=>{
        setActivo(!activo);
        form.resetFields()

    }

    const onFinish = values=>{

        if(activo || edit){
            
            values.fechadeIngreso=ingreso
            values.fechadeVencimiento=vencimiento
            values.formulacion=values.formulacion+ values.medida;
            delete values.medida;
        }else{
            values=null;
        }
        console.log(values)
        sendValues(values)
    }
    const onFinishFailed = ()=>{
        console.log('NO')
    }

    const anterior =()=>{
        minusCount();
        prev();
    }

    const prefixSelector = (
        <Form.Item name='medida' initialValue={edit?detalle.formulacion.slice(-2):'mg'} noStyle>
          <Dropbox style={{ width: 70 }}>
            <Option value="mg">mg</Option>
            <Option value="gr">gr</Option>
            <Option value="ml">ml</Option>
            <Option value="lt">lt</Option>
            <Option value="oz">oz</Option>
          </Dropbox>
        </Form.Item>
      );
    

    return (
    
        
                <Form   
                    style={{textAlign:"center",margin:'10px'}}
                    name="basic"
                    
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    form={form}
                    
                >  


                    {!edit &&
                        <Row type="flex" justify="center" align="middle">
                        
                                <Form.Item className='form-item'   style={{textAlign:'center'}}>
                                        <Switch checkedChildren="Nuevo Lote" unCheckedChildren="Nuevo Lote"  onChange={nuevo}/>
                                </Form.Item>
                            
                        </Row>
                    
                    }


                
                    {activo || edit?
                        <Row type="flex" justify="center" align="middle">
                            <Col span={24}>
                                
                                    {!edit&&
                                        <Form.Item 
                                            name="codigoCaja"
                                            rules={[
                                                {
                                                required: true,
                                                message: 'Ingrese el codigo de caja ',
                                                },
                                            ]}
                                        >

                                            <Input
                                                placeholder="Codigo de caja"
                                            />
                                            
                                        </Form.Item >
                                    }


                                        
                                                
                                        <Form.Item  
                                            initialValue={detalle &&moment(detalle.fechadeIngreso)}
                                            name="fechadeIngreso"
                                            rules={[
                                                {
                                                required: true,
                                                message: 'Ingrese la fecha de  Ingreso',
                                                },
                                            ]}
                                        >
                                            <DatePicker onChange={(date, dateString)=>setIngreso(dateString)} style={{minWidth:'200px'}}   placeholder='Fecha de ingreso' />
                                        </Form.Item>



                                        <Form.Item  
                                            name="fechadeVencimiento"
                                            initialValue={detalle && moment(detalle.fechadeVencimiento)}
                                            rules={[
                                                {
                                                required: true,
                                                message: 'Ingrese la fecha de Vencimiento',
                                                },
                                            ]}
                                        >
                                            <DatePicker onChange={(date, dateString)=>setVencimiento(dateString)}  placeholder='Fecha de Vencimiento' style={{minWidth:'200px',}} />
                                        </Form.Item>
                                            
                                        

                                    <Row type="flex" justify="center" align="middle">
                                        
                                            <Col style={{textAlign: "center"}} xs={24} sm={24} md={9} lg={9}>
                                                <Form.Item 
                                                    initialValue={detalle && detalle.tipo.tipo}
                                                    name="tipo"
                                                    
                                                    rules={[
                                                        {
                                                        required: true,
                                                        message: 'Escoja una Presentacion',
                                                        },
                                                    ]}
                                                >
                                                    <Select
                                                       
                                                        value={select.presentacion}
                                                        displayEmpty
                                                        onChange={(e)=>{setSelect({...select, presentacion:e.target.value})}}
                                                        inputProps={{
                                                            classes:{
                                                                root: classes.root,
                                                                icon:classes.icon
                                                            }
                                                        }}
                                                        className={classes.underline}
                                                        style={{minWidth:'135px',textAlign: "center"}}
                                                        renderValue={
                                                            select.presentacion !== "" ? undefined : () => detalle?detalle.tipo.tipo: <Placeholder>Presentaciones</Placeholder>
                                                        }
                                                        >
                                                            <MenuItem disabled>
                                                                    presentaciones
                                                            </MenuItem>
                                                            {presentaciones.presentaciones.map(presentacion=>(
                                                                <MenuItem key={presentacion._id} value={presentacion.tipo}>
                                                                    {presentacion.tipo}
                                                                </MenuItem>
                                                            ))}
                                                    </Select>
                                                </Form.Item>
                                            </Col>

                                            <Col className='margin-left-10px' style={{textAlign: "center"}} xs={24} sm={24} md={9} lg={9}>
                                                <Form.Item 

                                                    initialValue={detalle && detalle.cantidad}
                                                    name="cantidad"
                                                    rules={[
                                                        {
                                                        required: true,
                                                        message: 'Ingrese una Cantidad',
                                                        },
                                                    ]}
                                                >
                                                    <InputNumber
                                                        placeholder='Cantidad'
                                                        style={{minWidth:'135px',textAlign: "center"}}
                                                        parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                                    />
                                                </Form.Item>
                                            </Col>
                                    </Row>

                                    <Row type="flex" justify="center" align="middle">
                                        <Col  style={{textAlign: "center"}} xs={24} sm={24} md={9} lg={9}>
                                            <Form.Item 
                                                initialValue={detalle && detalle.preciodeCompra}
                                                name="preciodeCompra"
                                                rules={[
                                                    {
                                                    required: true,
                                                    message: 'Ingrese una precio de compra',
                                                    },
                                                ]}
                                            >
                                                <InputNumber
                                                    placeholder='Precio de compra'
                                                    style={{minWidth:'135px',textAlign: "center"}}
                                                    
                                                    parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                                    step={0.1}
                                                />
                                            </Form.Item>        
                                        </Col>
                                        <Col  style={{textAlign: "center"}} xs={24} sm={24} md={9} lg={9}>        
                                            <Form.Item 
                                                initialValue={detalle && detalle.preciodeVenta}
                                                name="preciodeVenta"
                                                rules={[
                                                    {
                                                    required: true,
                                                    message: 'Ingrese una precio de compra',
                                                    },
                                                ]}
                                            >
                                                <InputNumber
                                                    placeholder='Precio de venta'
                                                    style={{minWidth:'135px',textAlign: "center"}}
                                                    parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                                    step={0.1}
                                                />
                                            </Form.Item>
                                        </Col>
                                    </Row>

                                    <Row type="flex" justify="center" align="middle">
                                        <Col  style={{textAlign: "center"}} xs={24} sm={24} md={9} lg={9}>
                                            <Form.Item 
                                                initialValue={detalle && detalle.limitedeMeses}
                                                name="limitedeMeses"
                                                rules={[
                                                    {
                                                    required: true,
                                                    message: 'Ingrese una precio de compra',
                                                    },
                                                ]}
                                            >
                                                <InputNumber
                                                    placeholder='Limite de meses'
                                                    style={{minWidth:'135px',textAlign: "center"}}
                                                    parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                                />
                                            </Form.Item>    
                                        </Col>
                                        <Col  style={{textAlign: "center"}} xs={24} sm={24} md={9} lg={9}>
                                            <Form.Item 
                                                initialValue={detalle && detalle.laboratorio._id}
                                                name="laboratorio"
                                                
                                                rules={[
                                                    {
                                                    required: true,
                                                    message: 'Escoja una Laboratorio',
                                                    },
                                                ]}
                                            >
                                                <Select
                                                    
                                                    value={select.laboratorio}
                                                    displayEmpty
                                                    onChange={(e)=>{setSelect({...select,laboratorio:e.target.value})}}
                                                    inputProps={{
                                                        classes:{
                                                            root: classes.root,
                                                            icon:classes.icon
                                                        }
                                                    }}
                                                    className={classes.underline}
                                                    style={{minWidth:'135px',textAlign: "center"}}
                                                    renderValue={
                                                        select.laboratorio !== "" ? undefined : () =>detalle?detalle.laboratorio.nombreLaboratorio: <Placeholder>laboratorios</Placeholder>
                                                    }
                                                    >
                                                        <MenuItem disabled>
                                                                laboratorio
                                                        </MenuItem>
                                                        {laboratorios.laboratorios.map(laboratorio=>(
                                                            <MenuItem key={laboratorio._id} value={laboratorio._id}>
                                                                {laboratorio.nombreLaboratorio}
                                                            </MenuItem>
                                                        ))}
                                                </Select>
                                            </Form.Item>
                                        </Col>
                                    </Row>

                                    <Form.Item
                                        name="formulacion"
                                        rules={[{ required: true, message: 'Porfavor ingrese la formulacion' }]}
                                        initialValue={detalle && detalle.formulacion.slice(0,-2)}
                                    >

                                        <Input  style={{width: "180px"}} placeholder='Formulación' type='number' onChange={(e)=>console.log(e.target.value)} addonAfter={prefixSelector}  />
                                    </Form.Item>
                                    
                                    <Form.Item 
                                        initialValue={detalle && detalle.metodoImportacion}
                                        name="metodoImportacion"
                                        
                                        rules={[
                                            {
                                            required: true,
                                            message: 'Escoja una metodo de Importacion',
                                            },
                                        ]}
                                    >
                                        <Select
                                            
                                            value={select.importacion}
                                            displayEmpty
                                            onChange={(e)=>{setSelect({...select,importacion:e.target.value})}}
                                            inputProps={{
                                                classes:{
                                                    root: classes.root,
                                                    icon:classes.icon
                                                }
                                            }}
                                            className={classes.underline}
                                            style={{minWidth:'135px',textAlign: "center"}}
                                            renderValue={
                                                select.importacion !== "" ? undefined : () => detalle?detalle.metodoImportacion: <Placeholder>Importacion</Placeholder>
                                            }
                                            >
                                                <MenuItem disabled>
                                                        Importacion
                                                </MenuItem>
                                                <MenuItem value='Avion'>
                                                        Avion
                                                </MenuItem>
                                                <MenuItem value='Barco'>
                                                        Barco
                                                </MenuItem>
                                        </Select>
                                    </Form.Item>                     
                                    
                            </Col>

                             

                        </Row>
                    :
                        <h1>Todo Listo <CheckCircleOutlined style={{color:'green'}} /></h1>
                    }     
                             
                        
                       


                    <Row type="flex" justify="center" align="middle">
                        <Form.Item >
                        {!edit&&
                            <Button htmlType="button"  onClick={anterior}>
                                Anterior
                            </Button>
                        }  
                            <Button style={{marginLeft:'10px'}}  type="primary" htmlType="submit">
                                Ingresar
                            </Button>                            
                            
                        </Form.Item>
                    </Row>
                </Form>

    );
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(DetallesForm));