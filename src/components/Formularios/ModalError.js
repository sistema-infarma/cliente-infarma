import React from 'react';
import {Row} from 'antd';
const ModalError = ({children}) => {
    return (
        <Row type="flex" justify="center" align="middle">
            <h2 style={{color:'red'}}>{children}</h2>
        </Row>
    );
};

export default ModalError;