import React,{useState,useEffect} from 'react';

import { Form, Input, Button,Row,Col,DatePicker,InputNumber} from 'antd';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles } from '@material-ui/core/styles';
import {getPresentaciones,getLaboratorios} from '../../redux/ActionCreators'
import {withRouter} from 'react-router-dom';
import { connect } from 'react-redux';


const mapStateToProps=state=>{
    return {
        presentaciones:state.presentaciones,
        laboratorios:state.laboratorios
    }
}

const mapDispatchToProps=(dispatch)=>({
    getPresentaciones:()=>{dispatch(getPresentaciones())},
    getLaboratorios:()=>{dispatch(getLaboratorios())}

}); 


const useStyles = makeStyles(theme => ({
    underline: {
         '&&&:before':{
            border:'none'
         },
          '&&:after':{
            borderColor:'#1890FF' 
        },
        '&:hover':{
            border:'none'
        }
    },
    root:{
        fontFamily:'Segoe UI',
        fontSize:'14px',
        border: '1px solid #D9D9D9',
        padding:'5px 24px 6px 0px'
    },
    icon:{
        fill: '#D9D9D9'
    },
    gray:{
        color:'#BFBFBF'
            
    }
}))


const Placeholder=({children})=>{
    const classes=useStyles();
    return <div className={classes.gray}>{children}</div>;
}

const AddDetalles = ({presentaciones,laboratorios,getPresentaciones,getLaboratorios}) => {
    const classes=useStyles();


    const [form] = Form.useForm();
    const [select, setSelect] = useState("");
    const [vencimiento,setVencimiento]=useState('')
    const [ingreso,setIngreso]=useState('')
    const option =(e)=>{
        console.log(select)
        setSelect(e.target.value);
    }

    const onFinish = values=>{
        
        console.log(values)
    }
    const onFinishFailed = ()=>{
        console.log('NO')
    }

    useEffect(()=>{
        getPresentaciones();
        getLaboratorios();
    },[])

    return (
        <Form
            style={{textAlign:"center",margin:'10px'}}
            name="basic"
            initialValues={{
            remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            form={form}
        >
        
                    <Form.Item 
                        name="codigoCaja"
                        rules={[
                            {
                            required: true,
                            message: 'Ingrese el codigo de caja ',
                            },
                        ]}
                    >

                        <Input
                            placeholder="Codigo de caja"
                        />
                        
                    </Form.Item >


                        
                                
                        <Form.Item  
                            
                            name="fechadeIngreso"
                            rules={[
                                {
                                required: true,
                                message: 'Ingrese la fecha de  Ingreso',
                                },
                            ]}
                        >
                            <DatePicker onChange={(date, dateString)=>setIngreso(dateString)} style={{minWidth:'200px'}}   placeholder='Fecha de ingreso' />
                        </Form.Item>



                        <Form.Item  
                            name="fechadeVencimiento"
                            rules={[
                                {
                                required: true,
                                message: 'Ingrese la fecha de Vencimiento',
                                },
                            ]}
                        >
                            <DatePicker onChange={(date, dateString)=>setVencimiento(dateString)}  placeholder='Fecha de Vencimiento' style={{minWidth:'200px',}} />
                        </Form.Item>
                            
                        

                    <Row type="flex" justify="center" align="middle">
                        
                            <Col style={{textAlign: "center"}} sm={24} md={9} lg={9}>
                                <Form.Item 
                                    name="tipo"
                                    
                                    rules={[
                                        {
                                        required: true,
                                        message: 'Escoja una Presentacion',
                                        },
                                    ]}
                                >
                                    <Select
                                        
                                        value={select}
                                        displayEmpty
                                        onChange={option}
                                        inputProps={{
                                            classes:{
                                                root: classes.root,
                                                icon:classes.icon
                                            }
                                        }}
                                        className={classes.underline}
                                        style={{minWidth:'135px',textAlign: "center"}}
                                        renderValue={
                                            select !== "" ? undefined : () => <Placeholder>Presentaciones</Placeholder>
                                        }
                                        >
                                            <MenuItem disabled>
                                                    presentaciones
                                            </MenuItem>
                                            {presentaciones.presentaciones.map(presentacion=>(
                                                <MenuItem key={presentacion._id} value={presentacion.tipo}>
                                                    {presentacion.tipo}
                                                </MenuItem>
                                            ))}
                                    </Select>
                                </Form.Item>
                            </Col>

                            <Col className='margin-left-10px' style={{textAlign: "center"}}  sm={24} md={9} lg={9}>
                                <Form.Item 
                                    name="cantidad"
                                    rules={[
                                        {
                                        required: true,
                                        message: 'Ingrese una Cantidad',
                                        },
                                    ]}
                                >
                                    <InputNumber
                                        placeholder='Cantidad'
                                        style={{minWidth:'135px',textAlign: "center"}}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                    />
                                </Form.Item>
                            </Col>
                    </Row>

                    <Row type="flex" justify="center" align="middle">
                        <Col style={{textAlign: "center"}} sm={24} md={9} lg={9}>
                            <Form.Item 
                                name="preciodeCompra"
                                rules={[
                                    {
                                    required: true,
                                    message: 'Ingrese una precio de compra',
                                    },
                                ]}
                            >
                                <InputNumber
                                    placeholder='Precio de compra'
                                    style={{minWidth:'135px',textAlign: "center"}}
                                    
                                    parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                    step={0.1}
                                />
                            </Form.Item>        
                        </Col>
                        <Col className='margin-left-10px' style={{textAlign: "center"}} sm={24} md={9} lg={9}>        
                            <Form.Item 
                                name="preciodeVenta"
                                rules={[
                                    {
                                    required: true,
                                    message: 'Ingrese una precio de compra',
                                    },
                                ]}
                            >
                                <InputNumber
                                    placeholder='Precio de venta'
                                    style={{minWidth:'135px',textAlign: "center"}}
                                    parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                    step={0.1}
                                />
                            </Form.Item>
                        </Col>
                    </Row>

                    <Row type="flex" justify="center" align="middle">
                        <Col style={{textAlign: "center"}} sm={24} md={9} lg={9}>
                            <Form.Item 
                                name="limitedeMeses"
                                rules={[
                                    {
                                    required: true,
                                    message: 'Ingrese una precio de compra',
                                    },
                                ]}
                            >
                                <InputNumber
                                    placeholder='Limite de meses'
                                    style={{minWidth:'135px',textAlign: "center"}}
                                    parser={value => value.replace(/\$\s?|(,*)/g, '')}
                                />
                            </Form.Item>    
                        </Col>
                        <Col className='margin-left-10px' style={{textAlign: "center"}} sm={24} md={9} lg={9}>
                            <Form.Item 
                                name="laboratorio"
                                
                                rules={[
                                    {
                                    required: true,
                                    message: 'Escoja una Presentacion',
                                    },
                                ]}
                            >
                                <Select
                                    
                                    value={select}
                                    displayEmpty
                                    onChange={option}
                                    inputProps={{
                                        classes:{
                                            root: classes.root,
                                            icon:classes.icon
                                        }
                                    }}
                                    className={classes.underline}
                                    style={{minWidth:'135px',textAlign: "center"}}
                                    renderValue={
                                        select !== "" ? undefined : () => <Placeholder>laboratorios</Placeholder>
                                    }
                                    >
                                        <MenuItem disabled>
                                                laboratorio
                                        </MenuItem>
                                        {laboratorios.laboratorios.map(laboratorio=>(
                                            <MenuItem key={laboratorio._id} value={laboratorio._id}>
                                                {laboratorio.nombreLaboratorio}
                                            </MenuItem>
                                        ))}
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>

                    <Form.Item 
                        name="metodoImportacion"
                        
                        rules={[
                            {
                            required: true,
                            message: 'Escoja una metodo de Importacion',
                            },
                        ]}
                    >
                        <Select
                            
                            value={select}
                            displayEmpty
                            onChange={option}
                            inputProps={{
                                classes:{
                                    root: classes.root,
                                    icon:classes.icon
                                }
                            }}
                            className={classes.underline}
                            style={{minWidth:'135px',textAlign: "center"}}
                            renderValue={
                                select !== "" ? undefined : () => <Placeholder>Importacion</Placeholder>
                            }
                            >
                                <MenuItem disabled>
                                        Importacion
                                </MenuItem>
                                <MenuItem value='Avion'>
                                        Avion
                                </MenuItem>
                                <MenuItem value='Barco'>
                                        Barco
                                </MenuItem>
                        </Select>
                    </Form.Item>

                    
                        <Form.Item >
                            <Button style={{marginLeft:'10px'}} type="primary" htmlType="submit">
                                Ingresar
                            </Button>
                        </Form.Item>
                    

           </Form>
    );
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(AddDetalles));;