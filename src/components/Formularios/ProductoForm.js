import React,{useState} from 'react';

import {Carousel,Modal,message } from 'antd';
import GeneralForm from './GeneralForm'
import {ExclamationCircleOutlined } from '@ant-design/icons'

import '../../css/Layout.css'
import {postProducto} from '../../redux/ActionCreators'
import DetallesForm from './DetallesForm'
import ModalError from './ModalError'
import {withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
const {confirm}=Modal


const mapStateToProps=state=>{
    return {
        producto:state.producto
    }
}

const mapDispatchToProps=(dispatch)=>({
    postProducto:(v,history,message)=>{
        dispatch(postProducto(v,history,message)) 
    }
}); 

const ProductoForm = ({prev,next,back,postProducto,producto,history}) => {
    
    const [Producto,setProducto]=useState({
        producto:'',
        nombreComercial:'',
        descripcion:'',
        nombreGenerico:'',
        newActivo:''
    })


    let CarouselRef = React.createRef();

    const [count, setCount] = useState(1)

      const carouselChange=(e)=>{
          console.log(e)
            if(e+1<count){
               setCount(e+1);
    
            }else{
               setCount(e+1);
            }
      }    

      const showWarning=async(detalles)=>{
        confirm({
            title: 'Desea ingresar este producto?',
            icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
            okText:'Si',
            cancelText:'No',
            onCancel() {
            },
            onOk(){
                
                let product ={...Producto,detalles:{...detalles}}
                 postProducto(product,history,message)
            },
            
          });
      }
      const  aumentCount= (values)=>{
          console.log(count)
        switch(count){
            case 1:
                CarouselRef.current.next()
                setProducto({
                    producto:values.producto,
                    nombreComercial:values.nombreComercial,
                    descripcion:values.descripcion,
                    nombreGenerico:values.nombreGenerico,
                    newActivo:values.newActivo
                });
            break;

            case 2:
                
                 showWarning(values)
            break;
        }
      }

      const minusCount =()=>{
        
        CarouselRef.current.prev()
      }

        return(
            <React.Fragment>
                <Carousel ref={CarouselRef} dots={false} afterChange={carouselChange}>
                        <GeneralForm next={next} sendValues={aumentCount} buttomText='Siguiente'/>
                        <DetallesForm next={next} prev={prev} minusCount={minusCount} sendValues={aumentCount}/>
                </Carousel>
                {producto.errMess && 
                    <ModalError>{producto.errMess}</ModalError>
                }
            </React.Fragment>
        )
      

};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(ProductoForm));