import React from 'react';
import { Form, Input, Button,Row,Col,Switch} from 'antd';
import MaskedInput from 'antd-mask-input'
const LaboratorioForm = ({sendValues,lab}) => {

    
    return (
        <Form
            onFinish={sendValues}
            initialValues={lab && lab}
        >

            <Form.Item
                /* initialValue={product &&product.producto.nombreComercial} */
                name="nombreLaboratorio"
                initialValue={lab && lab.nombre}
                rules={[
                    {
                    required: true,
                    message: 'Escriba el nombre del Laboratorio',
                    },
                ]}
            >
                <Input
                    placeholder="Nombre"
                />
            </Form.Item>

            <Form.Item
                /* initialValue={product &&product.producto.nombreComercial} */
                name="numeroTelefono"
                initialValue={lab && lab.telefono}
                rules={[
                    {
                    required: true,
                    message: 'Escriba el numero del Laboratorio',
                    },
                ]}
            >
                <MaskedInput mask="1111-1111" placeholder="Telefono" size="20"/>
            </Form.Item>

            <Form.Item
                /* initialValue={product &&product.producto.nombreComercial} */
                name="correoLaboratorio"
                initialValue={lab && lab.correo}
                rules={[
                    {
                    required: true,
                    message: 'Escriba el correo del Laboratorio',
                    },
                ]}
            >
                <Input
                    placeholder="Correo"
                />
            </Form.Item>

            <Row type="flex" justify="center" align="middle">
                <Button  style={{marginBottom:'10px'}}  type="primary" htmlType="submit">
                    Ingresar
                </Button>     
            </Row>

        </Form>
    );
};

export default LaboratorioForm;