import React,{useState} from 'react';

import {Steps,Row,Col} from 'antd';
import ProductoForm from './ProductoForm';
const { Step } = Steps;
const NewProduct = () => {

    const [count, setCount] = useState(0)

    const steps = [
      {
        title: 'General'
      },
      {
        title: 'Detalles'
      }
    ];

     const next=()=>{

        const current = count + 1;
        setCount(current);
      }
    
    const prev=()=>{
        
        const current = count- 1;
        setCount(current);
      }
    const back=()=>{
        setCount(0);
    }
    


    return (
        <Row type="flex" justify="center" align="middle">
              <Col span={24} >         
                    <Steps style={{marginBottom:'30px'}} current={count}>
                        {steps.map(item => (
                            <Step key={item.title} title={item.title} />
                        ))}
                    </Steps>
                    
                    <ProductoForm next={next} prev={prev} back={back}/>
              </Col>
        </Row>
    );
};

export default NewProduct;