import React,{useState, useEffect} from 'react';
import { Form, Input, Button,Row,Col,Switch} from 'antd';

import {withRouter} from 'react-router-dom';
import { connect } from 'react-redux';

import Loading from '../Loading'

import { makeStyles } from '@material-ui/core/styles';

import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {getActivos} from '../../redux/ActionCreators'

const mapStateToProps=state=>{
    return {
        activos:state.activos
    }
}

const mapDispatchToProps=(dispatch)=>({
    getActivos:()=>{dispatch(getActivos())}
}); 


const useStyles = makeStyles(theme => ({
    underline: {
          '&:after':{
            borderColor:'#1890FF' 
          }
    }
}))

const GeneralForm = ({sendValues,next,getActivos,activos,buttomText,product}) => {

    const classes = useStyles();
    const [Option,setOption]=useState('');
    const [activo, setActivo] = useState(false)

    const [form] = Form.useForm();

    console.log(product)
    useEffect(() => {
       getActivos(); 
    }, [])

    const nuevo =()=>{
        setActivo(!activo);
        form.resetFields()
        console.log(!activo)
    }

    const option =(e)=>{
        console.log(e.target.value)
        setOption(e.target.value);
    }

    const onFinish = values => {
        values.newActivo=activo;
        console.log(values)
        sendValues(values);
        if(next){
            next()
        }
      };
    
      const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
      };

      if (activos.isLoading) return <Loading size={100} height={150}/>
    return (

            <Row type="flex" justify="center" align="middle" >
                <Col style={{textAlign:"center"}}span={24}>
                    <Form 
                        initialValues={product} 
                        name="basic"
                        initialValues={{
                        remember: true,
                        }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        form={form}
                        
                    >

                        
                            <Row type="flex" justify="center" align="middle">
                                <Form.Item >
                                        <Switch checkedChildren="Nuevo Ativo" unCheckedChildren="Nuevo Activo" onChange={nuevo}/>
                                </Form.Item>
                            </Row>
                        

                        {!activo ?
                            <Form.Item 
                                initialValue={product && product.producto._id }
                                classes={{select: classes.overrides}}
                                name="producto"
                                rules={[
                                    {
                                    required: true,
                                    message: 'Escoja un Activo',
                                    },
                                ]}
                            >
                                <Select 
                                value={undefined}
                                cheke
                                className={classes.underline}
                                defaultValue={product && product.producto._id }
                                displayEmpty
                                onChange={option}
                                style={{minWidth:'200px',textAlign: "center"}}>
                                    <MenuItem value={undefined} disabled>
                                        Activo
                                    </MenuItem>
                                    {activos.activos.map(activo=>(
                                        <MenuItem key={activo._id} value={activo._id}>
                                            {activo.nombreComercial}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </Form.Item>
                            :
                            <React.Fragment>
                                <Form.Item
                                    initialValue={product &&product.producto.nombreComercial}
                                    name="nombreComercial"
                                    rules={[
                                        {
                                        required: true,
                                        message: 'Escriba el nombre del Activo',
                                        },
                                    ]}
                                >
                                    <Input
                                        placeholder="Activo"
                                    />
                                </Form.Item>

                                <Form.Item
                                    initialValue={product &&product.producto.descripcion}
                                    name="descripcion"
                                    rules={[
                                        {
                                        required: true,
                                        message: 'Escriba la descripcion del Activo',
                                        },
                                    ]}
                                >
                                    <Input
                                        placeholder="Descripcion"                            
                                    />
                                </Form.Item>
 
                            </React.Fragment> 
                        }

                        <Form.Item
                            initialValue={product &&product.nombreGenerico}
                            name="nombreGenerico"
                            rules={[
                                {
                                required: true,
                                message: 'Ingrese el nombre Generico ',
                                },
                            ]}
                        >
                            <Input
                                placeholder="Generico"
                            />
                        </Form.Item>  
                        

                        <Form.Item >
                            <Button type="primary" htmlType="submit">
                                {buttomText}
                            </Button>
                        </Form.Item>
                    </Form>
                </Col>
            </Row>
    );
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(GeneralForm));