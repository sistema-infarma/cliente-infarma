import React from 'react';
import {Row,Spin} from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
const Loading = ({size,height}) => {
    const antIcon = <LoadingOutlined style={{ fontSize: size? size:200 }} spin />
    return (
        <Row type="flex" justify="center" align="middle" style={{minHeight:height?height:'90vh'}}>
                <Spin indicator={antIcon} />
        </Row>
    );
};

export default Loading;