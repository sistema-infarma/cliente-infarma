import React,{useEffect,useState,useRef} from 'react';
import {getProductos,putGenerico} from '../redux/ActionCreators';
import {withRouter, Link } from 'react-router-dom';
import StepsProducto from './Formularios/StepsProducto'
import { connect } from 'react-redux';
import Loading from './Loading';
import {Input,Table,Button,PageHeader ,Space,Row,Col,Switch,Modal,message} from 'antd';
import Highlighter from 'react-highlight-words';
import {SearchOutlined,PlusOutlined,CloseCircleOutlined,ExclamationCircleOutlined,CheckCircleOutlined } from '@ant-design/icons';
import ModalForm from './Formularios/ModalForm'
const mapStateToProps=state=>{
    return {
        productos:state.productos
    }
}

const mapDispatchToProps=(dispatch)=>({
    getProductos:()=>{dispatch(getProductos())},
    putProductos:(v,history,message)=>{dispatch(putGenerico(v,history,message))}
}); 

const {confirm}=Modal;
const Productos = (props) => {

    useEffect(()=>{
      props.getProductos();
    },[])

    

    console.log(props.productos.isLoading)
    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');
  
    const title=window.location.href.substring(27,window.location.href.length)
    const searchInput = useRef(null);
    const [eliminados,setEliminados]=useState(false)
    const [prods,setProds]=useState([])
    if(props.productos.isLoading)return <Loading/>
    let list
    list=props.productos.productos&&props.productos.productos.filter(p=>p.activo===true)

   const eliminated=()=>{
      setEliminados(!eliminados)
      console.log(eliminados)
      if(!eliminados){
        setProds(props.productos.productos)
      }else{
        setProds(props.productos.productos.filter(p=>p.activo===true));
      }
   }

   const eliminate=(genericoId)=>{
      confirm({
        title: 'Desea eliminar este producto?',
        icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
        okText:'Si',
        cancelText:'No',
        onCancel() {
        },
        onOk(){
            let values={};
            values.activo=false;
            values.genericoId=genericoId
            props.putProductos(values,props.history,message)
        },
        
      });
    }
   
    const activate=(genericoId)=>{
      confirm({
        title: 'Desea activar este producto?',
        icon: <ExclamationCircleOutlined style={{color:'#ffa200'}} />,
        okText:'Si',
        cancelText:'No',
        onCancel() {
        },
        onOk(){
          let values={};
          values.activo=true;
          values.genericoId=genericoId
          props.putProductos(values,props.history,message)
        },
        
      });
    }

   const getColumnSearchProps= (dataIndex)=> {
        return {
          filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
              <Input

                ref={ searchInput }
                placeholder={`Search ${dataIndex}`}
                value={selectedKeys[0]}
                onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                style={{ width: 188, marginBottom: 8, display: 'block' }}
              />
              <Space>
                <Button
                  type="primary"
                  onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                  icon={<SearchOutlined />}
                  size="small"
                  style={{ width: 90 }}
                >
                  Search
                </Button>
                <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                  Reset
                </Button>
              </Space>
            </div>
          ),
          filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
          onFilter: (value, record) =>
            record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
          onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => searchInput.current.select());
            }
          },
          render: text =>
            searchedColumn === dataIndex ? (
              <Highlighter
                highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                searchWords={[searchText]}
                autoEscape
                textToHighlight={text.toString()}
              />
            ) : (
              text
            ),
        }
      };

      const handleSearch=(selectedKeys, confirm, dataIndex)=>{
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
      };
    
      const handleReset=(clearFilters)=>{
        clearFilters();
        setSearchText('');
      };
    


    const tablaIncrustrada =(detalles)=>{
        console.log(detalles)


        const Nestedcolumns=[
            {
                title: 'Codigo',
                dataIndex: 'codigo',
                key: 'codigo'
            },
            {
                title: 'Presentacion',
                dataIndex: 'presentacion',
                key: 'presentacion',
                ...getColumnSearchProps('presentacion')
            },
            {
                title: 'Cantidad',
                dataIndex: 'cantidad',
                key: 'cantidad'
            },
            {
                title: 'Precio',
                dataIndex: 'precio',
                key: 'precio'
            },
            {
              title: 'Formulación',
              dataIndex: 'formulacion',
              key: 'formulacion',
              ...getColumnSearchProps('formulacion')
          },
            {
                title:'Laboratorio',
                dataIndex: 'laboratorio',
                key: 'laboratorio'
            }
        ]
        let nestedData=[];
            console.log(detalles)
            detalles.forEach(detalle=>{
                console.log(detalle.activo)
                nestedData.push({
                    key:detalle._id,
                    codigo:detalle.codigoProductoPresentacion,
                    presentacion:detalle.tipo.tipo,
                    cantidad:detalle.cantidad,
                    precio:detalle.preciodeVenta,
                    laboratorio:detalle.laboratorio.nombreLaboratorio,
                    formulacion:detalle.formulacion,
                    activo:detalle.activo
                })
            })
        console.log(nestedData)

        return <Table columns={Nestedcolumns} rowClassName={(record, index)=>(!record.activo && 'red')} dataSource={nestedData} pagination={false} />;
    }
    
        const columns=[
            {
                title: 'Nombre',
                dataIndex: 'nombre',
                key: 'nombre',
                ...getColumnSearchProps('nombre')
            },
            {
                title: 'Activo',
                dataIndex: 'activo',
                key: 'activo', 
                ...getColumnSearchProps('activo')
            },
            {
                title: 'Descripcion',
                dataIndex: 'descripcion',
                key: 'descripcion', 
            },
            {
                title: 'Detalles',
                key: 'detalles',
                render: (text, record) => (
                  <Space size="middle">
                    <Link to={`/productos/${record.key}`}>Ver mas</Link>
                  </Space>
                ),
              },
            {
              title: 'Eliminar',
              key: 'eliminar',
              render: (text, record) => (
                <Space style={{textAlign:'center'}} size="middle">
                  {record.active ?
                  <Button onClick={()=>(eliminate(record.key))} type="text" style={{marginLeft:'30%'}}  shape="circle" icon={<CloseCircleOutlined style={{fontSize:'30px',color:'red'}} />}  />:
                  <Button onClick={()=>(activate(record.key))} type="text" style={{marginLeft:'30%'}}  shape="circle" icon={<CheckCircleOutlined style={{fontSize:'30px',color:'green'}} />}  />
                  }
                </Space>
              ),
            }
        ]
        let rows=[]
        console.log( props.productos.productos)
        if(prods.length>0){
          prods.forEach(producto=>{
              rows.push({
                      key:producto._id,
                      nombre:producto.nombreGenerico,
                      activo:producto.producto.nombreComercial,
                      descripcion:producto.producto.descripcion,
                      detalles:producto.detalles,
                      active:producto.activo
                  })
          })
        }else{
          list.forEach(producto=>{
              rows.push({
                      key:producto._id,
                      nombre:producto.nombreGenerico,
                      activo:producto.producto.nombreComercial,
                      descripcion:producto.producto.descripcion,
                      detalles:producto.detalles,
                      active:producto.activo
                  })
          })
        }


    return (
        <React.Fragment>
              <Row type="flex" justify="right" align="middle">
                  
                      <PageHeader
                          className="font-title"
                          title={(title).charAt(0).toUpperCase()+title.slice(1)}
                      />
                       
                      <ModalForm title='Nuevo Producto'><StepsProducto/> </ModalForm>
                      <Switch onChange={eliminated} style={{marginLeft:'auto',marginRight:'10px'}} checkedChildren="Eliminados" unCheckedChildren="Eliminados" />
                  
              </Row>

              <Table pagination={{pageSize:6}}  rowClassName={(record, index)=>(!record.active && 'red')} columns={columns} dataSource={rows} scroll={{ x: 600}} expandedRowRender={record=>tablaIncrustrada(record.detalles)} />
        </React.Fragment>
    );
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Productos));