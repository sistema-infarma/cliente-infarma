import React,{useEffect} from 'react';
import {getProducto} from '../redux/ActionCreators';
import {withRouter} from 'react-router-dom';
import {Row,Col} from 'antd'
import { connect } from 'react-redux';
import Loading from './Loading'
import '../css/producto.css'

import Generico from './Producto/Generico';
import Activo from './Producto/Activo'
import Detalles from './Producto/Detalles'
const mapStateToProps=state=>{
    return {
        producto:state.producto
    }
}

const mapDispatchToProps=(dispatch)=>({
    getProducto:(id)=>{dispatch(getProducto(id))}
}); 


const Producto = (props) => {
      
    useEffect(()=>{
        props.getProducto(props.id)
    }, []);

    if(props.producto.isLoading) return <Loading/>
    console.log(props.producto)
    const producto=props.producto.producto
    const activo =props.producto.producto.producto;
    const detalles=props.producto.producto.detalles;

    return (
        <Row type="flex" justify="center" align="middle">
            <Col span={24}>
                <Row className='product-bg' type="flex" justify="center" align="middle">
                    <Col span={24}>
                        <Generico producto={producto}/>
                        <Activo activo ={activo}/>
                    </Col>
                </Row>
                <Row  type="flex" justify="center" align="middle" style={{backgroundColor:'white'}}>
                    <Col span={24}>
                        <Detalles detalles={detalles} idProducto={producto._id}/>
                    </Col>
                </Row>
            </Col>
        </Row>

    );
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Producto));