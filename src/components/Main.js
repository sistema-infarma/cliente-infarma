import React from 'react';

import {loginUser,logoutUser} from '../redux/ActionCreators'

import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import Login from './Login'
import Dashboard from './Dashboard'

const mapStateToProps=state=>{
    return {
        auth:state.auth
    }
}

const mapDispatchToProps=(dispatch)=>({
    loginUser:(creds,history)=>{dispatch(loginUser(creds,history))},
    logoutUser:()=>{dispatch(logoutUser())}
});


const Main = (props) => {

    const PrivateRoute = ({ component: Component, ...rest }) => (
        <Route {...rest} render={(Componentprops) => (
          props.auth.isAuthenticated
            ? <Component {...Componentprops} />
            : <Redirect to={{
                pathname: '/login',
                state: { from: props.location }
              }} />
        )} />
      );


      const login=()=>{
        return(
            <Login loginUser={props.loginUser} history={props.history} auth={props.auth}/>
        )
    }
    
      const dashboard=()=>{
          return(
              <Dashboard logoutUser={props.logoutUser} PrivateRoute={PrivateRoute} auth={props.auth}/>
          );
      }

    return (
        <TransitionGroup>
            <CSSTransition key={props.location.key} classNames="fade" timeout={300}>
                <Switch>
                    <PrivateRoute exact path='/dashboard' component={dashboard}/>
                    <Route exact path='/login' render={()=>(
                        props.auth.isAuthenticated?
                        <Redirect to="/dashboard"/>:
                        login()
                    )}/>

                    <Route exact path='/login' component={login}/>
                    <Redirect to='/dashboard'/>
                </Switch>
            </CSSTransition>
        </TransitionGroup>
    );
};

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Main)) ;