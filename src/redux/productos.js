import * as ActionTypes from './ActionTypes';


export const Productos =(state={
    isLoading:true,
    errMess:null,
    productos:[]
},action)=>{
    switch(action.type){
        case ActionTypes.ADD_PRODUCTOS:
            return {
                ...state,
                isLoading:false,
                errMess:null,
                productos: action.payload
            }
        case ActionTypes.LOADING_PRODUCTOS:
            return{
                ...state,
                isLoading:true,
                errMess:null,
                productos:[]
            }
        case ActionTypes.FAILED_PRODUCTOS:
            return {
                ...state,
                isLoading: true,
                errMess: action.payload,
                productos:[]
            }
        default:return state
    }
}