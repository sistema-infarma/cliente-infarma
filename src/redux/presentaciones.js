import * as ActionTypes from './ActionTypes';


export const Presentaciones =(state={
    isLoading:true,
    errMess:null,
    presentaciones:[]
},action)=>{
    switch(action.type){
        case ActionTypes.ADD_PRESENTACIONES:
            return {
                ...state,
                isLoading:false,
                errMess:null,
                presentaciones: action.payload
            }
        case ActionTypes.LOADING_PRESENTACIONES:
            return{
                ...state,
                isLoading:true,
                errMess:null,
                presentaciones:[]
            }
        case ActionTypes.FAILED_PRESENTACIONES:
            return {
                ...state,
                isLoading: true,
                errMess: action.payload,
                presentaciones:[]
            }
        default:return state
    }
}