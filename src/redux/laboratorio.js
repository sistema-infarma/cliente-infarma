

import * as ActionTypes from './ActionTypes';

export const Laboratorio =(state={
    isLoading:true,
    errMess:null,
    laboratorio:{}
},action)=>{
    switch(action.type){
        case ActionTypes.POST_LABORATORIO:
            return {
                ...state,
                isLoading: false,
                errMess: null,
                laboratorio:action.payload
            }
        case ActionTypes.POST_FAILED_LABORATORIO:
            return {
                ...state,
                errMess: action.payloads.response.data
            }
        case ActionTypes.PUT_LABORATORIO:
            return {
                ...state,
                isLoading: false,
                errMess: null,
                laboratorio:action.payload
            }
        case ActionTypes.PUT_FAILED_LABORATORIO:
            return {
                ...state,
                errMess: action.payloads.response.data
            }

            default:return state
    }
}