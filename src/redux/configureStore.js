
import {Auth} from './auth';
import {Productos} from './productos'
import {Producto} from './producto'
import {Activos} from './activos'
import {Presentaciones} from './presentaciones'
import {Laboratorios} from './laboratorios'
import {Laboratorio} from './laboratorio'
import{Presentacion} from './presentacion'
import {Activo} from './activo';
import {createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { createForms } from 'react-redux-form';
import { InitialFeedback } from './forms';

export const ConfigureStore=()=>{
    const store = createStore(
        combineReducers({
            auth:Auth,
            productos:Productos,
            producto:Producto,
            activos:Activos,
            activo:Activo,
            presentaciones:Presentaciones,
            presentacion:Presentacion,
            laboratorios:Laboratorios,
            laboratorio:Laboratorio,
            ...createForms({
                feedback: InitialFeedback
            })
        }),
        applyMiddleware(thunk,logger)
    );

    return store
}