import * as ActionTypes from './ActionTypes';

export const Activo =(state={
    isLoading:true,
    errMess:null,
    activo:{}
},action)=>{
    switch(action.type){
        case ActionTypes.POST_ACTIVO:
            return {
                ...state,
                isLoading: false,
                errMess: null,
                activo:action.payload
            }
        case ActionTypes.POST_FAILED_ACTIVO:
            return {
                ...state,
                errMess: action.payloads.response.data
            }
        case ActionTypes.PUT_ACTIVO:
            return {
                ...state,
                isLoading: false,
                errMess: null,
                activo:action.payload
            }
        case ActionTypes.PUT_FAILED_ACTIVO:
            return {
                ...state,
                errMess: action.payloads.response.data
            }

            default:return state
    }
}