import * as ActionTypes from './ActionTypes';


export const Laboratorios =(state={
    isLoading:true,
    errMess:null,
    laboratorios:[]
},action)=>{

    switch(action.type){
        case ActionTypes.ADD_LABORATORIOS:
            return {
                ...state,
                isLoading:false,
                errMess:null,
                laboratorios: action.payload
            }
        case ActionTypes.LOADING_LABORATORIOS:
            return{
                ...state,
                isLoading:true,
                errMess:null,
                laboratorios:[]
            }
        case ActionTypes.FAILED_LABORATORIOS:
            return {
                ...state,
                isLoading: true,
                errMess: action.payload,
                laboratorios:[]
            }
        default:return state
    }
}