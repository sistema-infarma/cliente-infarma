import * as ActionTypes from './ActionTypes';


export const Activos =(state={
    isLoading:true,
    errMess:null,
    activos:[]
},action)=>{

    switch(action.type){
        case ActionTypes.ADD_ACTIVOS:
            return {
                ...state,
                isLoading:false,
                errMess:null,
                activos: action.payload
            }
        case ActionTypes.LOADING_ACTIVOS:
            return{
                ...state,
                isLoading:true,
                errMess:null,
                activos:[]
            }
        case ActionTypes.FAILED_ACTIVOS:
            return {
                ...state,
                isLoading: true,
                errMess: action.payload,
                activos:[]
            }
        default:return state
    }
}