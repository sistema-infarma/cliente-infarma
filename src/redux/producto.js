import * as ActionTypes from './ActionTypes';

export const Producto =(state={
    isLoading:true,
    errMess:null,
    producto:{}
},action)=>{
    switch(action.type){
        case ActionTypes.ADD_PRODUCTO:
            return {
                ...state,
                isLoading:false,
                errMess:null,
                producto: action.payload
            }
        case ActionTypes.POST_PRODUCTO:
            return {
                ...state,
                isLoading:false,
                errMess:null,
                producto: action.payload
            }
        case ActionTypes.POST_FAILED_PRODUCTO:
            return {
                ...state,
                errMess: action.payloads.response.data
            }
        case ActionTypes.PUT_PRODUCTO_GENERICO:
            return {
                ...state,
                isLoading:false,
                errMess:null
            }
        case ActionTypes.PUT_PRODUCTO_ACTIVO:
            return {
                ...state,
                isLoading:false,
                errMess:null
            }
        case ActionTypes.PUT_DETALLE_GENERICO:
            return {
                ...state,
                isLoading:false,
                errMess:null
            }
        case ActionTypes.LOADING_PRODUCTO:
            return{
                ...state,
                isLoading:true,
                errMess:null
            }
        case ActionTypes.FAILED_PRODUCTO:
            return {
                ...state,
                isLoading: false,
                errMess: action.payloads.response.data,
                producto:{}
            }
        case ActionTypes.PUT_FAILED_PRODUCTO:
            return {
                ...state,
                errMess: action.payloads.response.data
            }
        default:return state
    }
}