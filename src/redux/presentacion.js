

import * as ActionTypes from './ActionTypes';

export const Presentacion =(state={
    isLoading:true,
    errMess:null,
    presentacion:{}
},action)=>{
    switch(action.type){
        case ActionTypes.POST_PRESENTACION:
            return {
                ...state,
                isLoading: false,
                errMess: null,
                presentacion:action.payload
            }
        case ActionTypes.POST_FAILED_PRESENTACION:
            return {
                ...state,
                errMess: action.payloads.response.data
            }
        case ActionTypes.PUT_PRESENTACION:
            return {
                ...state,
                isLoading: false,
                errMess: null,
                presentacion:action.payload
            }
        case ActionTypes.PUT_FAILED_PRESENTACION:
            return {
                ...state,
                errMess: action.payloads.response.data
            }

            default:return state
    }
}