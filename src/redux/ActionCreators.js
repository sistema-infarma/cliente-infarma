import * as ActionTypes from './ActionTypes';
import {Link} from '../config';
import axios from 'axios';
import {message} from 'antd';



//LOGIN
const requestLogin=(values)=>{
    return {
        type: ActionTypes.LOGIN_REQUEST,
        values
    }
}

const receiveLogin=(response)=>{
    return{
        type:ActionTypes.LOGIN_SUCCESS,
        token:response.token,
        admin:response.admin
    }
}
const loginError = (message) => {
    return {
        type: ActionTypes.LOGIN_FAILURE,
        message
    }
}

export const  loginUser=(values,history)=> (dispatch)=>{
    console.log(values)

    dispatch(requestLogin(values));
    return axios.post(Link + 'users/login',JSON.stringify(values), {
        headers: { 
            'Content-Type':'application/json' 
        }
    }).then(response=>{
        console.log(response)
            if (response.status===200) {
                return response.data;
            } else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },error => {
            throw error;
        }).then(response=>{
                localStorage.setItem('token', response.token);
                localStorage.setItem('values', JSON.stringify(values));
                localStorage.setItem('admin',response.admin);
                dispatch(receiveLogin(response))
                history.push('/dashboard');
        }).catch(e=>dispatch(loginError(e)))
}

//LOGOUT

export const requestLogout = () => {
    return {
      type: ActionTypes.LOGOUT_REQUEST
    }
}
  
export const receiveLogout = () => {
    return {
      type: ActionTypes.LOGOUT_SUCCESS
    }
}

export const logoutUser = () => (dispatch) => {
    dispatch(requestLogout())
    localStorage.removeItem('token');
    localStorage.removeItem('values');
    dispatch(receiveLogout())
}



//************************************************************************** */

//PRODUCTOS

//************************************************************************** */

const loadingProductos=()=>({
    type: ActionTypes.LOADING_PRODUCTOS
})

const addProductos=(productos)=>({
    type:ActionTypes.ADD_PRODUCTOS,
    payload:productos
})

const productosFailed=(err)=>({
    type:ActionTypes.FAILED_PRODUCTOS,
    payloads:err
})

export const getProductos=()=>(dispatch)=>{

    dispatch(loadingProductos());
    return axios.get(Link+'productos/genericos').then(response=>{
        if (response.status===200) {
            console.log(response.data)
            dispatch(addProductos(response.data))
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },error => {
        throw error;
    }).catch(e=>dispatch(productosFailed(e)))
}


const loadingProducto=()=>({
    type: ActionTypes.LOADING_PRODUCTO
})

const addProducto=(producto)=>({
    type:ActionTypes.ADD_PRODUCTO,
    payload:producto
})

const productoFailed=(err)=>({
    type:ActionTypes.FAILED_PRODUCTO,
    payloads:err
})

export const getProducto =(id)=>(dispatch)=>{
    dispatch(loadingProducto());
    return axios.get(`${Link}productos/genericos/${id}`).then(response=>{
        console.log(response)
        if (response.status===200) {
            dispatch(addProducto(response.data))
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },error => {
        throw error;
    }).catch(e=>dispatch(productoFailed(e)))
}



const postProductoFailed=(err)=>({
    type:ActionTypes.POST_FAILED_PRODUCTO,
    payloads:err
})

const postingProducto=(producto)=>({
    type:ActionTypes.POST_PRODUCTO,
    payload:producto
})

export const postProducto=(value,history,message)=>async(dispatch)=>{
    console.log(JSON.stringify(value));
    let endpoint=''
    if(!value.newActivo){
        endpoint='genericos'
    }
    return await axios.post(`${Link}productos/${endpoint}`,JSON.stringify(value), {
        headers: { 
            'Content-Type':'application/json'
        }
    }).then(response=>{
        console.log(response)
        if (response.status===200) {
            dispatch(postingProducto(response.data))
            message.success('Producto ingresado correctamente')
                
            history.push('/dashboard');
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
        return response
    },error => {
        throw error;
    }).catch(e=>dispatch(postProductoFailed(e)))
}

const putProductoFailed=(err)=>({
    type:ActionTypes.PUT_FAILED_PRODUCTO,
    payloads:err
})


const putingGenerico=(producto)=>({
    type:ActionTypes.PUT_PRODUCTO_GENERICO,
    payload:producto
})

export const putGenerico=(value,history,message)=>(dispatch)=>{
    console.log(JSON.stringify(value));
   
    return axios.put(`${Link}productos/genericos/${value.genericoId}`,JSON.stringify(value), {
        headers: { 
            'Content-Type':'application/json'
        }
    }).then(response=>{
        console.log(response)
        if (response.status===200) {
            console.log(response)
            dispatch(putingGenerico(response.data))
            message.success('Producto Modificado correctamente');
                
            history.push('/dashboard');
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },error => {
        throw error;
    }).catch(e=>dispatch(putProductoFailed(e)))
}

export const putGenericoDetalles=(value,history,message)=>(dispatch)=>{

    return axios.put(`${Link}productos/genericos/${value.genericoId}/detalles/${value.detalleId}`,JSON.stringify(value), {
        headers: { 
            'Content-Type':'application/json'
        }
    }).then(response=>{
        console.log(response)
        if (response.status===200) {
            console.log(response)
            dispatch(putingGenerico(response.data))
            message.success('Detalle Modificado correctamente');
                
            history.push('/dashboard');
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },error => {
        throw error;
    }).catch(e=>dispatch(putProductoFailed(e)))
}

const putingProducto=(producto)=>({
    type:ActionTypes.PUT_PRODUCTO_GENERICO,
    payload:producto
})

export const putProducto=(value,history,message)=>(dispatch)=>{
    console.log(JSON.stringify(value));
   
    return axios.put(`${Link}productos/${value.productoId}`,JSON.stringify(value), {
        headers: { 
            'Content-Type':'application/json'
        }
    }).then(response=>{
        console.log(response)
        if (response.status===200) {
            console.log(response)
            dispatch(putingProducto(response.data))
            if(value.genericoModified){
                putGenerico(value,history,message);
            }else{
                message.success('Producto Modificado correctamente')
                
                history.push('/dashboard');
            }
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },error => {
        throw error;
    }).catch(e=>dispatch(putProductoFailed(e)))
}


//************************************************************************** */

//ACTVOS

//************************************************************************** */

const loadingActivos=()=>({
    type: ActionTypes.LOADING_ACTIVOS
})

const addActivos=(productos)=>({
    type:ActionTypes.ADD_ACTIVOS,
    payload:productos
})

const activosFailed=(err)=>({
    type:ActionTypes.FAILED_ACTIVOS,
    payloads:err
})

export const getActivos =()=>(dispatch)=>{
    dispatch(loadingActivos);

    return axios.get(`${Link}productos`).then(response=>{
        console.log(response)
        if (response.status===200) {
            dispatch(addActivos(response.data))
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },error => {
        throw error;
    }).catch(e=>dispatch(activosFailed(e)))
}

const postActivoFailed=(err)=>({
    type:ActionTypes.POST_FAILED_ACTIVO,
    payloads:err
})

const postingActivo=(activo)=>({
    type:ActionTypes.POST_ACTIVO,
    payload:activo
})

export const postActivo=(value,history)=>async(dispatch)=>{
    console.log(JSON.stringify(value));
  
    return await axios.post(`${Link}productos`,JSON.stringify(value), {
        headers: { 
            'Content-Type':'application/json'
        }
    }).then(response=>{
        console.log(response)
        if (response.status===200) {
            dispatch(postingActivo(response.data))
            message.success('Activo ingresado correctamente')
                
            history.push('/dashboard');
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
        return response
    },error => {
        throw error;
    }).catch(e=>dispatch(postActivoFailed(e)))
}

const putActivoFailed=(err)=>({
    type:ActionTypes.PUT_FAILED_ACTIVO,
    payloads:err
})

const putingActivo=(laboratorio)=>({
    type:ActionTypes.PUT_ACTIVO,
    payload:laboratorio
})

export const putActivo=(value,history)=>(dispatch)=>{
    console.log(value)
    return  axios.put(`${Link}productos/${value.activoId}`,JSON.stringify(value), {
        headers: { 
            'Content-Type':'application/json'
        }
    }).then(response=>{
        console.log(response)
        if (response.status===200) {
            dispatch(putingActivo(response.data))
            message.success('Activo modificado correctamente')
            
            history.push('/dashboard');
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;

            throw error;      
        }
    },error => {
        throw error;
    }).catch(e=>dispatch(putActivoFailed(e)))
}

//************************************************************************** */

//LABORATORIOS

//************************************************************************** */

const loadingLaboratorios=()=>({
    type: ActionTypes.LOADING_LABORATORIOS
})

const addLaboratorios=(productos)=>({
    type:ActionTypes.ADD_LABORATORIOS,
    payload:productos
})

const LaboratoriosFailed=(err)=>({
    type:ActionTypes.FAILED_LABORATORIOS,
    payloads:err
})

export const getLaboratorios=()=>(dispatch)=>{
    dispatch(loadingLaboratorios)
    return axios.get(`${Link}laboratorios`).then(response=>{
        console.log(response)
        if (response.status===200) {
            dispatch(addLaboratorios(response.data))
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },error => {
        throw error;
    }).catch(e=>dispatch(LaboratoriosFailed(e)))
}

const postLabortatorioFailed=(err)=>({
    type:ActionTypes.POST_FAILED_LABORATORIO,
    payloads:err
})

const postingLabortatorio=(laboratorio)=>({
    type:ActionTypes.POST_LABORATORIO,
    payload:laboratorio
})

export const postLabortatorio=(value,history)=>async(dispatch)=>{
    console.log(JSON.stringify(value));
  
    return await axios.post(`${Link}laboratorios`,JSON.stringify(value), {
        headers: { 
            'Content-Type':'application/json'
        }
    }).then(response=>{
        console.log(response)
        if (response.status===200) {
            dispatch(postingLabortatorio(response.data))
            message.success('Labortatorio ingresado correctamente')
                
            history.push('/dashboard');
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
        return response
    },error => {
        throw error;
    }).catch(e=>dispatch(postLabortatorioFailed(e)))
}

const putLabortatorioFailed=(err)=>({
    type:ActionTypes.PUT_FAILED_LABORATORIO,
    payloads:err
})

const putingLabortatorio=(laboratorio)=>({
    type:ActionTypes.PUT_LABORATORIO,
    payload:laboratorio
})

export const putLabortatorio=(value,history)=>(dispatch)=>{
    console.log(value)
    return  axios.put(`${Link}laboratorios/${value.labId}`,JSON.stringify(value), {
        headers: { 
            'Content-Type':'application/json'
        }
    }).then(response=>{
        console.log(response)
        if (response.status===200) {
            dispatch(putingLabortatorio(response.data))
            message.success('Labortatorio modificado correctamente')
            
            history.push('/dashboard');
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;

            throw error;      
        }
    },error => {
        throw error;
    }).catch(e=>dispatch(putLabortatorioFailed(e)))
}

//************************************************************************** */

//PRESENTACIONES

//************************************************************************** */

const loadingPresentaciones=()=>({
    type: ActionTypes.LOADING_PRESENTACIONES
})

const addPresentaciones=(productos)=>({
    type:ActionTypes.ADD_PRESENTACIONES,
    payload:productos
})

const PresentacionesFailed=(err)=>({
    type:ActionTypes.FAILED_PRESENTACIONES,
    payloads:err
})

export const getPresentaciones=()=>(dispatch)=>{
    dispatch(loadingPresentaciones)
    return axios.get(`${Link}presentaciones`).then(response=>{
        console.log(response)
        if (response.status===200) {
            dispatch(addPresentaciones(response.data))
            return response.data;
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },error => {
        throw error;
    }).catch(e=>dispatch(PresentacionesFailed(e)))
}

const postPresentacionFailed=(err)=>({
    type:ActionTypes.POST_FAILED_PRESENTACION,
    payloads:err
})

const postingPresentacion=(laboratorio)=>({
    type:ActionTypes.POST_PRESENTACION,
    payload:laboratorio
})

export const postPresentacion=(value,history)=>async(dispatch)=>{
    console.log(JSON.stringify(value));
  
    return await axios.post(`${Link}presentaciones`,JSON.stringify(value), {
        headers: { 
            'Content-Type':'application/json'
        }
    }).then(response=>{
        console.log(response)
        if (response.status===200) {
            dispatch(postingPresentacion(response.data))
            message.success('Presentacion ingresada correctamente')
                
            history.push('/dashboard');
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
        return response
    },error => {
        throw error;
    }).catch(e=>dispatch(postPresentacionFailed(e)))
}

const putPresentacionFailed=(err)=>({
    type:ActionTypes.PUT_FAILED_PRESENTACION,
    payloads:err
})

const putingPresentacion=(laboratorio)=>({
    type:ActionTypes.PUT_PRESENTACION,
    payload:laboratorio
})

export const putPresentacion=(value,history)=>(dispatch)=>{
    console.log(value)
    return  axios.put(`${Link}presentaciones/${value.presentacionId}`,JSON.stringify(value), {
        headers: { 
            'Content-Type':'application/json'
        }
    }).then(response=>{
        console.log(response)
        if (response.status===200) {
            dispatch(putingPresentacion(response.data))
            message.success('Presentacion modificada correctamente')
            
            history.push('/dashboard');
        } else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;

            throw error;      
        }
    },error => {
        throw error;
    }).catch(e=>dispatch(putPresentacionFailed(e)))
}